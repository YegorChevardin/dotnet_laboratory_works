﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Button;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Task_7
{
    public partial class Task05 : Form
    {
        public Task05()
        {
            InitializeComponent();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
         
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void item12ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item13ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item14ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item15ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item16ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item17ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item111ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item112ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item113ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item114ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item115ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item21ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item23ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item24ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item25ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item26ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item27ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item221ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item222ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item223ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item224ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item225ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item33ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item32ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item34ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item35ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item37ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item331ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item332ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item333ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item334ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item335ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item361ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item362ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item363ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void item364ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showMessageBox(sender, e);
        }

        private void showMessageBox(object sender, EventArgs args)
        {
            if (sender is ToolStripMenuItem item)
            {
                MessageBox.Show(item.Text,
                "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
