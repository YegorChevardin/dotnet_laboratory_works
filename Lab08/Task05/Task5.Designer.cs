﻿namespace Task_7
{
    partial class Task05
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.item11ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item111ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item112ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item113ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item114ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item115ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item12ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item13ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item14ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item15ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item16ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item17ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item21ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item22ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item221ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item222ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item223ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item224ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item225ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item23ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item24ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item25ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item26ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item27ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item33ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item32ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item33ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.item331ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item332ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item333ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item334ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item335ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item34ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item35ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item36ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item361ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item362ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item363ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item364ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item37ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.item2ToolStripMenuItem,
            this.item3ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.item11ToolStripMenuItem,
            this.item12ToolStripMenuItem,
            this.item13ToolStripMenuItem,
            this.item14ToolStripMenuItem,
            this.item15ToolStripMenuItem,
            this.item16ToolStripMenuItem,
            this.item17ToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(49, 20);
            this.toolStripMenuItem1.Text = "Item1";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // item11ToolStripMenuItem
            // 
            this.item11ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.item111ToolStripMenuItem,
            this.item112ToolStripMenuItem,
            this.item113ToolStripMenuItem,
            this.item114ToolStripMenuItem,
            this.item115ToolStripMenuItem});
            this.item11ToolStripMenuItem.Name = "item11ToolStripMenuItem";
            this.item11ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item11ToolStripMenuItem.Text = "Item11";
            // 
            // item111ToolStripMenuItem
            // 
            this.item111ToolStripMenuItem.Name = "item111ToolStripMenuItem";
            this.item111ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item111ToolStripMenuItem.Text = "Item111";
            this.item111ToolStripMenuItem.Click += new System.EventHandler(this.item111ToolStripMenuItem_Click);
            // 
            // item112ToolStripMenuItem
            // 
            this.item112ToolStripMenuItem.Name = "item112ToolStripMenuItem";
            this.item112ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item112ToolStripMenuItem.Text = "Item112";
            this.item112ToolStripMenuItem.Click += new System.EventHandler(this.item112ToolStripMenuItem_Click);
            // 
            // item113ToolStripMenuItem
            // 
            this.item113ToolStripMenuItem.Name = "item113ToolStripMenuItem";
            this.item113ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item113ToolStripMenuItem.Text = "Item113";
            this.item113ToolStripMenuItem.Click += new System.EventHandler(this.item113ToolStripMenuItem_Click);
            // 
            // item114ToolStripMenuItem
            // 
            this.item114ToolStripMenuItem.Name = "item114ToolStripMenuItem";
            this.item114ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item114ToolStripMenuItem.Text = "Item114";
            this.item114ToolStripMenuItem.Click += new System.EventHandler(this.item114ToolStripMenuItem_Click);
            // 
            // item115ToolStripMenuItem
            // 
            this.item115ToolStripMenuItem.Name = "item115ToolStripMenuItem";
            this.item115ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item115ToolStripMenuItem.Text = "Item115";
            this.item115ToolStripMenuItem.Click += new System.EventHandler(this.item115ToolStripMenuItem_Click);
            // 
            // item12ToolStripMenuItem
            // 
            this.item12ToolStripMenuItem.Name = "item12ToolStripMenuItem";
            this.item12ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item12ToolStripMenuItem.Text = "Item12";
            this.item12ToolStripMenuItem.Click += new System.EventHandler(this.item12ToolStripMenuItem_Click);
            // 
            // item13ToolStripMenuItem
            // 
            this.item13ToolStripMenuItem.Name = "item13ToolStripMenuItem";
            this.item13ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item13ToolStripMenuItem.Text = "Item13";
            this.item13ToolStripMenuItem.Click += new System.EventHandler(this.item13ToolStripMenuItem_Click);
            // 
            // item14ToolStripMenuItem
            // 
            this.item14ToolStripMenuItem.Name = "item14ToolStripMenuItem";
            this.item14ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item14ToolStripMenuItem.Text = "Item14";
            this.item14ToolStripMenuItem.Click += new System.EventHandler(this.item14ToolStripMenuItem_Click);
            // 
            // item15ToolStripMenuItem
            // 
            this.item15ToolStripMenuItem.Name = "item15ToolStripMenuItem";
            this.item15ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item15ToolStripMenuItem.Text = "Item15";
            this.item15ToolStripMenuItem.Click += new System.EventHandler(this.item15ToolStripMenuItem_Click);
            // 
            // item16ToolStripMenuItem
            // 
            this.item16ToolStripMenuItem.Name = "item16ToolStripMenuItem";
            this.item16ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item16ToolStripMenuItem.Text = "Item16";
            this.item16ToolStripMenuItem.Click += new System.EventHandler(this.item16ToolStripMenuItem_Click);
            // 
            // item17ToolStripMenuItem
            // 
            this.item17ToolStripMenuItem.Name = "item17ToolStripMenuItem";
            this.item17ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item17ToolStripMenuItem.Text = "Item17";
            this.item17ToolStripMenuItem.Click += new System.EventHandler(this.item17ToolStripMenuItem_Click);
            // 
            // item2ToolStripMenuItem
            // 
            this.item2ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.item21ToolStripMenuItem,
            this.item22ToolStripMenuItem,
            this.item23ToolStripMenuItem,
            this.item24ToolStripMenuItem,
            this.item25ToolStripMenuItem,
            this.item26ToolStripMenuItem,
            this.item27ToolStripMenuItem});
            this.item2ToolStripMenuItem.Name = "item2ToolStripMenuItem";
            this.item2ToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.item2ToolStripMenuItem.Text = "Item2";
            // 
            // item21ToolStripMenuItem
            // 
            this.item21ToolStripMenuItem.Name = "item21ToolStripMenuItem";
            this.item21ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item21ToolStripMenuItem.Text = "Item21";
            this.item21ToolStripMenuItem.Click += new System.EventHandler(this.item21ToolStripMenuItem_Click);
            // 
            // item22ToolStripMenuItem
            // 
            this.item22ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.item221ToolStripMenuItem,
            this.item222ToolStripMenuItem,
            this.item223ToolStripMenuItem,
            this.item224ToolStripMenuItem,
            this.item225ToolStripMenuItem});
            this.item22ToolStripMenuItem.Name = "item22ToolStripMenuItem";
            this.item22ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item22ToolStripMenuItem.Text = "Item22";
            // 
            // item221ToolStripMenuItem
            // 
            this.item221ToolStripMenuItem.Name = "item221ToolStripMenuItem";
            this.item221ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item221ToolStripMenuItem.Text = "Item221";
            this.item221ToolStripMenuItem.Click += new System.EventHandler(this.item221ToolStripMenuItem_Click);
            // 
            // item222ToolStripMenuItem
            // 
            this.item222ToolStripMenuItem.Name = "item222ToolStripMenuItem";
            this.item222ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item222ToolStripMenuItem.Text = "Item222";
            this.item222ToolStripMenuItem.Click += new System.EventHandler(this.item222ToolStripMenuItem_Click);
            // 
            // item223ToolStripMenuItem
            // 
            this.item223ToolStripMenuItem.Name = "item223ToolStripMenuItem";
            this.item223ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item223ToolStripMenuItem.Text = "Item223";
            this.item223ToolStripMenuItem.Click += new System.EventHandler(this.item223ToolStripMenuItem_Click);
            // 
            // item224ToolStripMenuItem
            // 
            this.item224ToolStripMenuItem.Name = "item224ToolStripMenuItem";
            this.item224ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item224ToolStripMenuItem.Text = "Item224";
            this.item224ToolStripMenuItem.Click += new System.EventHandler(this.item224ToolStripMenuItem_Click);
            // 
            // item225ToolStripMenuItem
            // 
            this.item225ToolStripMenuItem.Name = "item225ToolStripMenuItem";
            this.item225ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item225ToolStripMenuItem.Text = "Item225";
            this.item225ToolStripMenuItem.Click += new System.EventHandler(this.item225ToolStripMenuItem_Click);
            // 
            // item23ToolStripMenuItem
            // 
            this.item23ToolStripMenuItem.Name = "item23ToolStripMenuItem";
            this.item23ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item23ToolStripMenuItem.Text = "Item23";
            this.item23ToolStripMenuItem.Click += new System.EventHandler(this.item23ToolStripMenuItem_Click);
            // 
            // item24ToolStripMenuItem
            // 
            this.item24ToolStripMenuItem.Name = "item24ToolStripMenuItem";
            this.item24ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item24ToolStripMenuItem.Text = "Item24";
            this.item24ToolStripMenuItem.Click += new System.EventHandler(this.item24ToolStripMenuItem_Click);
            // 
            // item25ToolStripMenuItem
            // 
            this.item25ToolStripMenuItem.Name = "item25ToolStripMenuItem";
            this.item25ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item25ToolStripMenuItem.Text = "Item25";
            this.item25ToolStripMenuItem.Click += new System.EventHandler(this.item25ToolStripMenuItem_Click);
            // 
            // item26ToolStripMenuItem
            // 
            this.item26ToolStripMenuItem.Name = "item26ToolStripMenuItem";
            this.item26ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item26ToolStripMenuItem.Text = "Item26";
            this.item26ToolStripMenuItem.Click += new System.EventHandler(this.item26ToolStripMenuItem_Click);
            // 
            // item27ToolStripMenuItem
            // 
            this.item27ToolStripMenuItem.Name = "item27ToolStripMenuItem";
            this.item27ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item27ToolStripMenuItem.Text = "Item27";
            this.item27ToolStripMenuItem.Click += new System.EventHandler(this.item27ToolStripMenuItem_Click);
            // 
            // item3ToolStripMenuItem
            // 
            this.item3ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.item33ToolStripMenuItem,
            this.item32ToolStripMenuItem,
            this.item33ToolStripMenuItem1,
            this.item34ToolStripMenuItem,
            this.item35ToolStripMenuItem,
            this.item36ToolStripMenuItem,
            this.item37ToolStripMenuItem});
            this.item3ToolStripMenuItem.Name = "item3ToolStripMenuItem";
            this.item3ToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.item3ToolStripMenuItem.Text = "Item3";
            // 
            // item33ToolStripMenuItem
            // 
            this.item33ToolStripMenuItem.Name = "item33ToolStripMenuItem";
            this.item33ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item33ToolStripMenuItem.Text = "Item31";
            this.item33ToolStripMenuItem.Click += new System.EventHandler(this.item33ToolStripMenuItem_Click);
            // 
            // item32ToolStripMenuItem
            // 
            this.item32ToolStripMenuItem.Name = "item32ToolStripMenuItem";
            this.item32ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item32ToolStripMenuItem.Text = "Item32";
            this.item32ToolStripMenuItem.Click += new System.EventHandler(this.item32ToolStripMenuItem_Click);
            // 
            // item33ToolStripMenuItem1
            // 
            this.item33ToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.item331ToolStripMenuItem,
            this.item332ToolStripMenuItem,
            this.item333ToolStripMenuItem,
            this.item334ToolStripMenuItem,
            this.item335ToolStripMenuItem});
            this.item33ToolStripMenuItem1.Name = "item33ToolStripMenuItem1";
            this.item33ToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.item33ToolStripMenuItem1.Text = "Item33";
            // 
            // item331ToolStripMenuItem
            // 
            this.item331ToolStripMenuItem.Name = "item331ToolStripMenuItem";
            this.item331ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item331ToolStripMenuItem.Text = "Item331";
            this.item331ToolStripMenuItem.Click += new System.EventHandler(this.item331ToolStripMenuItem_Click);
            // 
            // item332ToolStripMenuItem
            // 
            this.item332ToolStripMenuItem.Name = "item332ToolStripMenuItem";
            this.item332ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item332ToolStripMenuItem.Text = "Item332";
            this.item332ToolStripMenuItem.Click += new System.EventHandler(this.item332ToolStripMenuItem_Click);
            // 
            // item333ToolStripMenuItem
            // 
            this.item333ToolStripMenuItem.Name = "item333ToolStripMenuItem";
            this.item333ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item333ToolStripMenuItem.Text = "Item333";
            this.item333ToolStripMenuItem.Click += new System.EventHandler(this.item333ToolStripMenuItem_Click);
            // 
            // item334ToolStripMenuItem
            // 
            this.item334ToolStripMenuItem.Name = "item334ToolStripMenuItem";
            this.item334ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item334ToolStripMenuItem.Text = "Item334";
            this.item334ToolStripMenuItem.Click += new System.EventHandler(this.item334ToolStripMenuItem_Click);
            // 
            // item335ToolStripMenuItem
            // 
            this.item335ToolStripMenuItem.Name = "item335ToolStripMenuItem";
            this.item335ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item335ToolStripMenuItem.Text = "Item335";
            this.item335ToolStripMenuItem.Click += new System.EventHandler(this.item335ToolStripMenuItem_Click);
            // 
            // item34ToolStripMenuItem
            // 
            this.item34ToolStripMenuItem.Name = "item34ToolStripMenuItem";
            this.item34ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item34ToolStripMenuItem.Text = "Item34";
            this.item34ToolStripMenuItem.Click += new System.EventHandler(this.item34ToolStripMenuItem_Click);
            // 
            // item35ToolStripMenuItem
            // 
            this.item35ToolStripMenuItem.Name = "item35ToolStripMenuItem";
            this.item35ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item35ToolStripMenuItem.Text = "Item35";
            this.item35ToolStripMenuItem.Click += new System.EventHandler(this.item35ToolStripMenuItem_Click);
            // 
            // item36ToolStripMenuItem
            // 
            this.item36ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.item361ToolStripMenuItem,
            this.item362ToolStripMenuItem,
            this.item363ToolStripMenuItem,
            this.item364ToolStripMenuItem});
            this.item36ToolStripMenuItem.Name = "item36ToolStripMenuItem";
            this.item36ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item36ToolStripMenuItem.Text = "Item36";
            // 
            // item361ToolStripMenuItem
            // 
            this.item361ToolStripMenuItem.Name = "item361ToolStripMenuItem";
            this.item361ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item361ToolStripMenuItem.Text = "Item361";
            this.item361ToolStripMenuItem.Click += new System.EventHandler(this.item361ToolStripMenuItem_Click);
            // 
            // item362ToolStripMenuItem
            // 
            this.item362ToolStripMenuItem.Name = "item362ToolStripMenuItem";
            this.item362ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item362ToolStripMenuItem.Text = "Item362";
            this.item362ToolStripMenuItem.Click += new System.EventHandler(this.item362ToolStripMenuItem_Click);
            // 
            // item363ToolStripMenuItem
            // 
            this.item363ToolStripMenuItem.Name = "item363ToolStripMenuItem";
            this.item363ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item363ToolStripMenuItem.Text = "Item363";
            this.item363ToolStripMenuItem.Click += new System.EventHandler(this.item363ToolStripMenuItem_Click);
            // 
            // item364ToolStripMenuItem
            // 
            this.item364ToolStripMenuItem.Name = "item364ToolStripMenuItem";
            this.item364ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item364ToolStripMenuItem.Text = "Item364";
            this.item364ToolStripMenuItem.Click += new System.EventHandler(this.item364ToolStripMenuItem_Click);
            // 
            // item37ToolStripMenuItem
            // 
            this.item37ToolStripMenuItem.Name = "item37ToolStripMenuItem";
            this.item37ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item37ToolStripMenuItem.Text = "Item37";
            this.item37ToolStripMenuItem.Click += new System.EventHandler(this.item37ToolStripMenuItem_Click);
            // 
            // Task05
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Task05";
            this.Text = "Task05";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem item11ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item12ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item13ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item14ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item15ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item16ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item17ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item21ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item22ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item23ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item24ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item25ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item26ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item27ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item33ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item32ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item33ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem item34ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item35ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item36ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item37ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item111ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item112ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item113ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item114ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item115ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item221ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item222ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item223ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item224ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item225ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item331ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item332ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item333ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item334ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item335ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item361ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item362ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item363ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item364ToolStripMenuItem;
    }
}

