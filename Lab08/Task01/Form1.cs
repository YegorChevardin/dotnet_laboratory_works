namespace Task01
{
    public partial class Form1 : Form
    {
        private int position1, position2, position3, position4;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            position1 = textBox1.Top;
            position2 = textBox2.Top;
            position3 = textBox3.Top;
            position4 = textBox4.Top;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";

            textBox1.Top = position1;
            textBox2.Top = position2;
            textBox3.Top = position3;
            textBox4.Top = position4;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Top += 20;
            textBox2.Top += 20;
            textBox3.Top += 20;
            textBox4.Top += 20;
        }
    }
}