﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                "Name: " + textBox2.Text + ", \n"
                + "Age: " + comboBox1.Text + ", \n"
                + "Gender: " + this.CheckedGender(groupBox1) + ", \n"
                + "Interests: " + this.interests(groupBox1) + ", \n"
                + "File: " + textBox3.Text + ", \n"
                + "Additional information: " + richTextBox1.Text + ", \n"
                , "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            comboBox1.Text = "";
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            setInterestsUnchecked();
            textBox3.Text = "";
        }

        private void setInterestsUnchecked()
        {
            foreach (Control control in groupBox1.Controls)
            {
                if (control is CheckBox check && check.Checked)
                {
                    check.Checked = false;
                }
            }
        }

        private string CheckedGender(Control container)
        {
            foreach (Control control in container.Controls)
            {
                if (control is RadioButton radioButton && radioButton.Checked)
                {
                    return radioButton.Text;
                }
            }
            return "No gender selected";
        }

        private string interests(Control container)
        {
            string result = "";

            foreach (Control control in container.Controls)
            {
                if (control is CheckBox checkBox && checkBox.Checked)
                {
                    result += checkBox.Text + ", ";
                }
            }

            return result;
        }
    }
}
