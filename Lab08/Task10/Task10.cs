﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void stripClick(object sender, EventArgs e)
        {
            showMessageBox(sender);
        }

        private void showMessageBox(object sender)
        {
            if (sender is ToolStripButton tool)
            {
                MessageBox.Show(tool.Text,
                "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
