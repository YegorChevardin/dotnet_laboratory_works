﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_5
{
    public partial class Form1 : Form
    {
        private int position1, position2, position3, position4;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<CheckBox> checkedCheckBoxes = this.GetSelectedCheckBox(groupBox1);

            foreach (CheckBox check in checkedCheckBoxes)
            {
                check.Top += 2;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            checkBox1.Top = position1;
            checkBox2.Top = position2;
            checkBox3.Top = position3;
            checkBox4.Top = position4;

            checkBox1.Checked = false;
            checkBox2.Checked = false;
            checkBox3.Checked = false;
            checkBox4.Checked = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            position1 = checkBox1.Top;
            position2 = checkBox2.Top;
            position3 = checkBox3.Top;
            position4 = checkBox4.Top;
        }

        private List<CheckBox> GetSelectedCheckBox(Control container)
        {
            List<CheckBox> list = new List<CheckBox>();
            foreach (Control control in container.Controls)
            {
                if (control is CheckBox checkBox && checkBox.Checked)
                {
                    list.Add(checkBox);
                }
            }
            return list;
        }
    }
}
