﻿namespace Task_8
{
    partial class Task7
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.item1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item11ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item12ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item13ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item14ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item15ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item21ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item22ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item23ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item24ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.item3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.item1ToolStripMenuItem,
            this.item2ToolStripMenuItem,
            this.item3ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(181, 92);
            // 
            // item1ToolStripMenuItem
            // 
            this.item1ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.item11ToolStripMenuItem,
            this.item12ToolStripMenuItem,
            this.item13ToolStripMenuItem,
            this.item14ToolStripMenuItem,
            this.item15ToolStripMenuItem});
            this.item1ToolStripMenuItem.Name = "item1ToolStripMenuItem";
            this.item1ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item1ToolStripMenuItem.Text = "Item1";
            // 
            // item11ToolStripMenuItem
            // 
            this.item11ToolStripMenuItem.Name = "item11ToolStripMenuItem";
            this.item11ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item11ToolStripMenuItem.Text = "Item11";
            this.item11ToolStripMenuItem.Click += new System.EventHandler(this.item11ToolStripMenuItem_Click);
            // 
            // item12ToolStripMenuItem
            // 
            this.item12ToolStripMenuItem.Name = "item12ToolStripMenuItem";
            this.item12ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item12ToolStripMenuItem.Text = "Item12";
            this.item12ToolStripMenuItem.Click += new System.EventHandler(this.item12ToolStripMenuItem_Click);
            // 
            // item13ToolStripMenuItem
            // 
            this.item13ToolStripMenuItem.Name = "item13ToolStripMenuItem";
            this.item13ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item13ToolStripMenuItem.Text = "Item13";
            this.item13ToolStripMenuItem.Click += new System.EventHandler(this.item13ToolStripMenuItem_Click);
            // 
            // item14ToolStripMenuItem
            // 
            this.item14ToolStripMenuItem.Name = "item14ToolStripMenuItem";
            this.item14ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item14ToolStripMenuItem.Text = "Item14";
            this.item14ToolStripMenuItem.Click += new System.EventHandler(this.item14ToolStripMenuItem_Click);
            // 
            // item15ToolStripMenuItem
            // 
            this.item15ToolStripMenuItem.Name = "item15ToolStripMenuItem";
            this.item15ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item15ToolStripMenuItem.Text = "Item15";
            this.item15ToolStripMenuItem.Click += new System.EventHandler(this.item15ToolStripMenuItem_Click);
            // 
            // item2ToolStripMenuItem
            // 
            this.item2ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.item21ToolStripMenuItem,
            this.item22ToolStripMenuItem,
            this.item23ToolStripMenuItem,
            this.item24ToolStripMenuItem});
            this.item2ToolStripMenuItem.Name = "item2ToolStripMenuItem";
            this.item2ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item2ToolStripMenuItem.Text = "Item2";
            // 
            // item21ToolStripMenuItem
            // 
            this.item21ToolStripMenuItem.Name = "item21ToolStripMenuItem";
            this.item21ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item21ToolStripMenuItem.Text = "Item21";
            this.item21ToolStripMenuItem.Click += new System.EventHandler(this.item21ToolStripMenuItem_Click);
            // 
            // item22ToolStripMenuItem
            // 
            this.item22ToolStripMenuItem.Name = "item22ToolStripMenuItem";
            this.item22ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item22ToolStripMenuItem.Text = "Item22";
            this.item22ToolStripMenuItem.Click += new System.EventHandler(this.item22ToolStripMenuItem_Click);
            // 
            // item23ToolStripMenuItem
            // 
            this.item23ToolStripMenuItem.Name = "item23ToolStripMenuItem";
            this.item23ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item23ToolStripMenuItem.Text = "Item23";
            this.item23ToolStripMenuItem.Click += new System.EventHandler(this.item23ToolStripMenuItem_Click);
            // 
            // item24ToolStripMenuItem
            // 
            this.item24ToolStripMenuItem.Name = "item24ToolStripMenuItem";
            this.item24ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item24ToolStripMenuItem.Text = "Item24";
            this.item24ToolStripMenuItem.Click += new System.EventHandler(this.item24ToolStripMenuItem_Click);
            // 
            // item3ToolStripMenuItem
            // 
            this.item3ToolStripMenuItem.Name = "item3ToolStripMenuItem";
            this.item3ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.item3ToolStripMenuItem.Text = "Item3";
            this.item3ToolStripMenuItem.Click += new System.EventHandler(this.item3ToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(152, 142);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(358, 157);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "label2";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(484, 264);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "label3";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // Task7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Task7";
            this.Text = "Task07";
            this.Load += new System.EventHandler(this.Task8_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem item1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item11ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item12ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item13ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item14ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item15ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item21ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item22ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item23ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item24ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem item3ToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

