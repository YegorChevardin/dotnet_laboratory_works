﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_8
{
    public partial class Task7 : Form
    {
        private Label currentLabel;


        public Task7()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            currentLabel = label1;
            openMenuStrip();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            currentLabel = label2;
            openMenuStrip();
        }

        private void Task8_Load(object sender, EventArgs e)
        {
        }

        private void item11ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IncreaseLabelFontSize(currentLabel, 1.0f);
        }

        private void item12ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IncreaseLabelFontSize(currentLabel, 1.0f);
        }

        private void item13ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IncreaseLabelFontSize(currentLabel, 1.0f);
        }

        private void item14ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IncreaseLabelFontSize(currentLabel, 1.0f);
        }

        private void item15ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IncreaseLabelFontSize(currentLabel, 1.0f);
        }

        private void item21ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IncreaseLabelFontSize(currentLabel, 1.0f);
        }

        private void item22ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IncreaseLabelFontSize(currentLabel, 1.0f);
        }

        private void item23ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IncreaseLabelFontSize(currentLabel, 1.0f);
        }

        private void item24ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IncreaseLabelFontSize(currentLabel, 1.0f);
        }

        private void item3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IncreaseLabelFontSize(currentLabel, 1.0f);
        }



        private void openMenuStrip()
        {
            this.contextMenuStrip1.Show(Cursor.Position);
        }

        private void label3_Click(object sender, EventArgs e)
        {
            currentLabel = label3;
            openMenuStrip();
        }

        private void IncreaseLabelFontSize(Label label, float increaseValue)
        {
            label.Font = new Font(label.Font.FontFamily, label.Font.Size + increaseValue, label.Font.Style);
        }
    }
}
