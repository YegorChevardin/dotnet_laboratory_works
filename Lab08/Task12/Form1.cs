﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task12
{
    public partial class Form1 : Form
    {
        private int default_milliseconds = 250;
        private Boolean timerControl = false;
        public Form1()
        {
            InitializeComponent();
            timer1.Interval = default_milliseconds;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                default_milliseconds = int.Parse(textBox1.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
            }
            timer1.Interval = default_milliseconds;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (button1.BackColor == Color.White)
            {
                button1.BackColor = Color.Blue;
            } else
            {
                button1.BackColor = Color.White;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (timerControl)
            {
                timerControl = false;
                timer1.Stop();
                button3.Text = "start timer";
            } else
            {
                timerControl = true;
                timer1.Start();
                button3.Text = "stop timer";
            }
        }
    }
}
