﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_4
{
    public partial class Form1 : Form
    {
        private int position1, position2, position3, position4, position5, position6, position7;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RadioButton checkedRadioButton1 = this.GetSelectedRadioButton(groupBox1);
            RadioButton checkedRadioButton2 = this.GetSelectedRadioButton(groupBox2);

            checkedRadioButton1.Top += 20;
            checkedRadioButton2.Top += 20;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            radioButton6.Checked = true;
            radioButton1.Checked = true;

            radioButton1.Top = position1;
            radioButton2.Top = position2;
            radioButton3.Top = position3;
            radioButton4.Top = position4;
            radioButton5.Top = position5;
            radioButton6.Top = position6;
            radioButton7.Top = position7;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            position1 = radioButton1.Top;
            position2 = radioButton2.Top;
            position3 = radioButton3.Top;
            position4 = radioButton4.Top;
            position5 = radioButton5.Top;
            position6 = radioButton6.Top;
            position7 = radioButton7.Top;
        }

        private RadioButton GetSelectedRadioButton(Control container)
        {
            foreach (Control control in container.Controls)
            {
                if (control is RadioButton radioButton && radioButton.Checked)
                {
                    return radioButton;
                }
            }
            return null;
        }
    }
}
