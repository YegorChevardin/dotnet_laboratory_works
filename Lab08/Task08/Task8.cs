﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_8
{
    public partial class Task8 : Form
    {
        private Label currentLabel;
        public Task8()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            currentLabel = label1;
            openMenuStrip();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            currentLabel = label2;
            openMenuStrip();
        }

        private void Task8_Load(object sender, EventArgs e)
        {
            
        }

        private void IncreaseLabelFontSize(float increaseValue)
        {
            currentLabel.Font = new Font(currentLabel.Font.FontFamily, currentLabel.Font.Size + increaseValue, currentLabel.Font.Style);
        }

        private void openMenuStrip()
        {
            this.contextMenuStrip1.Show(Cursor.Position);
        }

        private void label3_Click(object sender, EventArgs e)
        {
            currentLabel = label3;
            openMenuStrip();
        }

        private void stripClick(object sender, EventArgs e)
        {
            IncreaseLabelFontSize(1.0f);
        }
    }
}
