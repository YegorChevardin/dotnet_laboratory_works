﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task09
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            textBox1.ForeColor = Color.Red;
            button1.ForeColor = Color.Red;
        }

        private void textBox2_Enter(object sender, EventArgs e) {
            textBox2.ForeColor = Color.Yellow;
            button1.ForeColor = Color.Yellow;
        }

        private void textBox1_Leave(object sender, EventArgs e) {
            textBox1.ForeColor = Color.Black;
            button1.ForeColor = Color.Black;
        }

        private void textBox2_Leave(object sender, EventArgs e) {
            textBox2.ForeColor= Color.Black;
            button1.ForeColor = Color.Black;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
