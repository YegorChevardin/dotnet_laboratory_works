﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task01
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            performLabelText("Button 1 was clicked", sender, e);
        }

        private void performLabelText(string text, object sender, EventArgs e)
        {
            label1.Text = text;
            label1.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            performLabelText("Button 2 was clicked", sender, e);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            performLabelText("Button 3 was clicked", sender, e);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
