﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task08
{
    public partial class Task08 : Form
    {
        public Task08()
        {
            InitializeComponent();
        }

        private void Task08_Load(object sender, EventArgs e)
        {

        }

        private void label_MouseDown(object sender, MouseEventArgs e)
        {
            Label clickedLabel = sender as Label;

            if (clickedLabel != null && e.Button == MouseButtons.Left)
            {
                if (clickedLabel == label1)
                {
                    clickedLabel.ForeColor = Color.Red;
                } else if (clickedLabel == label2)
                {
                    clickedLabel.ForeColor = Color.Yellow;
                } else if (clickedLabel == label3)
                {
                    clickedLabel.ForeColor = Color.Green;
                }
            }
        }

        private void label_MouseUp(object sender, MouseEventArgs e)
        {
            Label clickedLabel = sender as Label;

            if (clickedLabel != null)
            {
                clickedLabel.ForeColor = Color.Black;
            }
        }
    }
}
