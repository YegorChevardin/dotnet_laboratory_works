﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task11
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            char pressedKey = (char)e.KeyCode;

            if ((e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete) || (pressedKey >= 'N' && pressedKey <= 'Z') || (pressedKey >= 'n' && pressedKey <= 'z'))
            {
                textBox1.ForeColor = Color.DarkGreen;
            }
            else
            {
                textBox1.ForeColor = Color.Black;
                e.SuppressKeyPress = true;
            }
        }
    }
}
