﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task06
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button_MouseEnter(object sender, EventArgs e)
        {
            Button clickedButton = sender as Button;

            if (clickedButton != null)
            {
                if (clickedButton == button1)
                {
                    clickedButton.ForeColor = Color.Red;
                } else if (clickedButton == button2)
                {
                    clickedButton.ForeColor = Color.Green;
                } else if (clickedButton == button3)
                {
                    clickedButton.ForeColor = Color.Purple;
                }
            }
        }

        private void button_MouseLeave(object sender, EventArgs e)
        {
            Button clickedButton = sender as Button;

            if (clickedButton != null)
            {
                clickedButton.ForeColor = Color.Black;
            }
        }
    }
}
