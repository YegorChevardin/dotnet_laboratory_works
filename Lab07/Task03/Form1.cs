﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task03
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void Label1_DblClick(object sender, EventArgs e)
        {
            if (label1.ForeColor == Color.Black)
            {
                label1.ForeColor = Color.Red;
            }
            else if (label1.ForeColor == Color.Red)
            {
                label1.ForeColor = Color.Black;
            }
        }

        private void Label2_DblClick(object sender, EventArgs e)
        {
            if (label2.ForeColor == Color.Black)
            {
                label2.ForeColor = Color.Yellow;
            }
            else if (label2.ForeColor == Color.Yellow)
            {
                label2.ForeColor = Color.Black;
            }
        }

        private void Label3_DblClick(object sender, EventArgs e)
        {
            if (label3.ForeColor == Color.Black)
            {
                label3.ForeColor = Color.Green;
            }
            else if (label3.ForeColor == Color.Green)
            {
                label3.ForeColor = Color.Black;
            }
        }
    }
}
