﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task12
{
    public partial class Form1 : Form
    {
        private string[] hungarianCities = {
    "Budapest",
    "Debrecen",
    "Szeged",
    "Miskolc",
    "Pécs",
    "Győr",
    "Székesfehérvár"
};

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.Items.AddRange(hungarianCities);
            listBox1.Items.AddRange(hungarianCities);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedCity = comboBox1.SelectedItem.ToString();
            MessageBox.Show("Ви вибрали місто: " + selectedCity, "Інформація", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedCity = listBox1.SelectedItem.ToString();
            MessageBox.Show("Ви вибрали місто: " + selectedCity, "Інформація", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
