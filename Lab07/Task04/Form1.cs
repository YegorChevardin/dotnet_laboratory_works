﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task04
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void performLabelChangeColor(object sender, EventArgs e)
        {
            Label clickedLabel = sender as Label;

            if (clickedLabel != null)
            {
                if (clickedLabel == label1)
                {
                    if (label1.ForeColor == Color.Black)
                    {
                        label1.ForeColor = Color.Red;
                    }
                    else if (label1.ForeColor == Color.Red)
                    {
                        label1.ForeColor = Color.Black;
                    }
                } else if (clickedLabel == label2)
                {
                    if (label2.ForeColor == Color.Black)
                    {
                        label2.ForeColor = Color.Yellow;
                    }
                    else if (label2.ForeColor == Color.Yellow)
                    {
                        label2.ForeColor = Color.Black;
                    }
                } else if (clickedLabel == label3)
                {
                    if (label3.ForeColor == Color.Black)
                    {
                        label3.ForeColor = Color.Green;
                    }
                    else if (label3.ForeColor == Color.Green)
                    {
                        label3.ForeColor = Color.Black;
                    }
                }
            }
        }
    }
}
