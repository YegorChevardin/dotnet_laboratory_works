﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task10
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox_Enter(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            if (textBox != null)
            {
                if (textBox == textBox1)
                {
                    textBox.ForeColor = Color.Red;
                    button1.ForeColor = Color.Red;
                } else if (textBox == textBox2)
                {
                    textBox.ForeColor = Color.Yellow;
                    button1.ForeColor = Color.Yellow;
                }
            }
        }

        private void textBox_Leave(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;

            if (textBox != null)
            {
                textBox.ForeColor = Color.Black;
                button1.ForeColor = Color.Black;
            }
        }
    }
}
