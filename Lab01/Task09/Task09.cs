﻿namespace Task09
{
    public static class Task09
    {
        public static void Main(string[] args)
        {
            short n;
            double x, y;
            short h;

            Console.WriteLine("|   x   |      y     |");
            Console.WriteLine("|-------|------------|");
            for (n = 0; n < 5; n++)
            {
                for (x = 0; x < 4; x += 0.25)
                {
                    if (x < 2)
                        y = Math.Sqrt(4 - Math.Pow(x - 2, 2));
                    else
                        y = 4 - x;

                    Console.Write($"| {x + n * 4:F2} | {y:F7} |");

                    h = (short)((y + 1) * 10);

                    if (y - 1 - h * 10 > 0.5)
                        h++;

                    for (; h > 0; h--)
                        Console.Write(" ");

                    Console.WriteLine("*");
                }
            }
        }
    }
}

