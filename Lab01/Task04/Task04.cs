﻿using static System.Math;

namespace Task04
{
    public static class Task04
    {
        public static void Main(string[] args)
        {
            /* параметры */
            double a = 12.5, b = 1.2, x = 0.0, t1 = 0.0, t2 = 0.0;

            count(a, b, x, t1, t2);
        }

        private static void count(double a, double b, double x, double t1, double t2)
        {
            try
            {
                Console.WriteLine("Введите x>");
                Console.WriteLine("x: ");
                x = double.Parse(Console.ReadLine());

                t1 = 1 / (6 * Pow(a, 3)) * Log((a + x) / (a - x)) + 1 / (2 * Pow(a, 3));
                t2 = 1 / (a * b) * Log((Tan(a * x) + b) / (Tan(a * x) - b));

                if (t1.Equals(Double.NaN) || t2.Equals(Double.NaN))
                {
                    throw new Exception("Параметр Х (x = " + x + ") не задовольняє умову, спробуйте знову");
                }

                Console.WriteLine("t1 = {0}, t2 = {1} | x = {2}", t1, t2, x);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                count(a, b, x, t1, t2);
            }
        }
    }
}