﻿using static System.Math;

namespace Task05
{
    public static class Task05
    {
        public static void Main(string[] args)
        {
            double x, y; /* координаты точки */
            /* ввод координат */
            Console.WriteLine("Введите координату x >");
            x = double.Parse(Console.ReadLine());
            
            Console.WriteLine("Введите координату y >");
            y = double.Parse(Console.ReadLine());
            
            /* вывод только что введенных значений */
            Console.WriteLine("x={0};  y={1}", x, y);
            
            /* проверка условий */
            if (((Sqrt(Pow(x, 2) + Pow(y, 2)) <= 1) && x <= 0 && y <= 0) 
                || ((x >= 0 && x <= 1) && (y >= 0 && y <= 1)))
            {
                Console.WriteLine("Точка попадает в область");
            }
            else
            {
                Console.WriteLine("Точка не попадает в область");
            }
        }
    }
}