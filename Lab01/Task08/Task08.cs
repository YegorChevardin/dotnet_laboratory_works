﻿using static System.Math;

namespace Task08
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Введіть кількість елементів для сумування: ");
            int numElements = int.Parse(Console.ReadLine());
            
            Console.Write("Введіть бажану точність (epsilon): ");
            double epsilon = double.Parse(Console.ReadLine());

            double sum = 0;
            double term;

            for (int n = 0; n < numElements; n++)
            {
                term = CalculateTerm(n);

                if (Abs(term) >= epsilon)
                {
                    sum += term;
                }
                else
                {
                    Console.WriteLine($"Cума з урахуванням epsilon = {sum:F7}");
                    break;
                }

                if (n == numElements - 1)
                {
                    Console.WriteLine($"Сума перших {numElements} елементів = {sum:F7}");
                    break;
                }
            }
        }
        
        private static double CalculateTerm(int n)
        {
            return Pow(-1, n) * (1.0 / (2 * (n + 1)));
        }
    }
}