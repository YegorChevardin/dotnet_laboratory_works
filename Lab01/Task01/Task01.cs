﻿namespace Task01
{
    public static class Task01
    {
        public static void Main(string[] args)
        {
            string material01, material02, material03;
            char type1, type2, type3;
            float[] humidity1 = new float[2];
            float[] humidity2 = new float[2];
            float[] humidity3 = new float[2];
            float coeficient01, coeficient02, coeficient03;

            Console.WriteLine("Task01");
            /* Введение фактических данных*/
            Console.WriteLine("1. Введите: вещество, тип, влажность, коефициент >");
            material01 = Console.ReadLine();
            type1 = char.Parse(Console.ReadLine());
            readHumidity(humidity1, 1);
            coeficient01 = float.Parse(Console.ReadLine());
            Console.WriteLine("2. Введите: вещество, тип, влажность, коефициент >");
            material02 = Console.ReadLine();
            type2 = char.Parse(Console.ReadLine());
            readHumidity(humidity2, 2);
            coeficient02 = float.Parse(Console.ReadLine());
            Console.WriteLine("3. Введите: вещество, тип, влажность, коефициент >");
            material03 = Console.ReadLine();
            type3 = char.Parse(Console.ReadLine());
            readHumidity(humidity3, 3);
            coeficient03 = float.Parse(Console.ReadLine());
            
            /* Вывод таблицы */
            /* вывод заголовков */
            Console.WriteLine("-----------------------------------------------\n");
            Console.WriteLine("|Коэффициенты теплопроводимости материаллов |\n");
            Console.WriteLine("|---------------------------------------------|\n");
            Console.WriteLine("| Вещество | Тип | Влажность |Коефициэент|\n");
            Console.WriteLine("|----------|-------|-----------|-------------|\n");
            /* вывод строк фактических данных */
            Console.WriteLine("| {0,9} | {1,1} | {2,10:P2}-{3,10:P2} | {4,8:F2} |", material01, type1, humidity1[0],
                humidity1[1], coeficient01);
            Console.WriteLine("| {0,9} | {1,1} | {2,10:P2}-{3,10:P2} | {4,8:F2} |", material02, type2, humidity2[0],
                humidity2[1], coeficient02);
            Console.WriteLine("| {0,9} | {1,1} | {2,10:P2}-{3,10:P2} | {4,8:F2} |", material03, type3, humidity3[0],
                humidity3[1], coeficient03);
            /* вывод примечаний */
            Console.WriteLine("|----------------------------------------------------------|\n");
            Console.WriteLine("| Примечание: М - металлы, Т - термоизоляционные материалы;|\n");
            Console.WriteLine("|             Д - другие материалы                         |\n");
            Console.WriteLine("------------------------------------------------------------\n");
        }

        private static void readHumidity(float[] array, int entityNumber)
        {
            Console.WriteLine(entityNumber + ". Введите минимальное значение влажности:");
            float min = float.Parse(Console.ReadLine());
            Console.WriteLine(entityNumber + ". Введите максимальное значение влажности:");
            float max = float.Parse(Console.ReadLine());

            array[0] = min;
            array[1] = max;
        }
    }
}