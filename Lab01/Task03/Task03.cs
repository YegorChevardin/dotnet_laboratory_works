﻿using static System.Math;

namespace Task03
{
    public static class Task03
    {
        public static void Main(string[] args)
        {
            /* параметры, которые вводятся */
            double x;
            /* параметры, которые задаются в программе */
            double a = 12.5, b = 1.2;
            double t1, t2; /* результаты */

            Console.WriteLine("Введите x>");
            Console.WriteLine("x: ");
            x = double.Parse(Console.ReadLine());
            
            t1 = 1 / (6 * Pow(a, 3)) * Log((a + x) / (a - x)) + 1 / (2 * Pow(a, 3));
            t2 = 1 / (a * b) * Log((Tan(a * x) + b) / (Tan(a * x) - b));
            
            Console.WriteLine("t1 = {0}, t2 = {1} | x = {2}", t1, t2, x);
        }
    }
}