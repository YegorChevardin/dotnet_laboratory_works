﻿using static System.Math;

namespace Task07
{
    public static class Task07
    {
        public static void Main(string[] args)
        {
            long n;
            double sum = 0;
            double term;
            const double eps = 0.000001;
            long k2 = 1;
            short k1 = 1;

            for (n = 0; ; n++)
            {
                term = Pow(-1, n) * (1.0 / (2 * (n + 1)));
            
                if (Abs(term) >= eps)
                    sum += term;
                else
                    break;

                if (n == 9)
                    Console.WriteLine($"Сумма 10 членов ряда = {sum:F7}");
            }

            Console.WriteLine($"Полная сумма ряда = {sum:F7}");
        }
    }
}