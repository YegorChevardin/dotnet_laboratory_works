﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task07
{
    public partial class Form1 : Form
    {
        private Dictionary<string, int> keyValuePairs = 
            new Dictionary<string, int>();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            keyValuePairs.Add(checkBox1.Text, 9000);
            keyValuePairs.Add(checkBox2.Text, 10000);
            keyValuePairs.Add(checkBox3.Text, 11000);
        }

        private List<String> extractPlaneName()
        {
            List<String> result = new List<String>();
            foreach (Control control in groupBox1.Controls)
            {
                if (control is CheckBox check && check.Checked)
                {
                    result.Add(check.Text);
                }
            }

            return result;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int plane_price = 0;
                foreach (String plane_name in extractPlaneName())
                {
                    plane_price += keyValuePairs[plane_name];
                }

                int count = 1;

                if (textBox1.Text.Length > 0)
                {
                    count = int.Parse(textBox1.Text.Trim());
                }

                int result = plane_price * count;
                label2.Text = result.ToString();
            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
