﻿using System;
using System.Windows.Forms;

namespace Task05
{
    public partial class Form1 : Form
    {
        private double price = 9000;
        private double additional_loggage = 2000;
        private double insurance = 4500;
        private double pet_place = 3000;
        private double eatting = 4500;
        private double additional_eating = 1000;
        private double uah_to_pln = 0.11;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            double total_price = price;

            if (checkBox1.Checked)
            {
                total_price += additional_loggage;
            }

            if (checkBox2.Checked)
            {
                total_price += insurance;
            }

            if (checkBox3.Checked)
            {
                total_price += pet_place;
            }

            if (checkBox4.Checked)
            {
                total_price += eatting;
            }

            if (checkBox5.Checked)
            {
                total_price = total_price * uah_to_pln;
                label3.Text = total_price.ToString() + " pln.";

                label5.Text = (additional_eating*uah_to_pln).ToString() + " pln.";
                label7.Text = (additional_eating * uah_to_pln * 0.1).ToString() + " pln.";

                total_price += additional_eating*uah_to_pln - (additional_eating *uah_to_pln * 0.1);
                label9.Text = total_price.ToString() + " pln.";
            } else {
                label3.Text = total_price.ToString() + " грн.";

                label5.Text = additional_eating.ToString() + " грн.";
                label7.Text = (additional_eating * 0.1).ToString() + " грн.";

                total_price += additional_eating - (additional_eating * 0.1);
                label9.Text = total_price.ToString() + " грн.";
            }
        }
    }
}
