﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task07
{
    public partial class Form1 : Form
    {
        private Dictionary<string, int> keyValuePairs = 
            new Dictionary<string, int>();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            keyValuePairs.Add(radioButton1.Text, 9000);
            keyValuePairs.Add(radioButton2.Text, 10000);
            keyValuePairs.Add(radioButton3.Text, 11000);
        }

        private String extractPlaneName()
        {
            foreach (Control control in groupBox1.Controls)
            {
                if (control is RadioButton radio && radio.Checked)
                {
                    return radio.Text;
                }
            }

            return "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                double plane_price = keyValuePairs[extractPlaneName()];
                int count = 1;

                if (textBox1.Text.Length > 0)
                {
                    count = int.Parse(textBox1.Text.Trim());
                }

                if (count > 3)
                {
                    plane_price = plane_price - plane_price * 0.1;
                }

                double result = plane_price * count;
                label2.Text = result.ToString();
            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
