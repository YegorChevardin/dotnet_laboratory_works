﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task01
{
    public partial class Form1 : Form
    {
        private double coeficient = 0.11;
        private bool to_uah = true;
        private string label_to_uah = "Сума у грн.";
        private string label_to_pln = "Сума у злотих";

        public Form1()
        {
            InitializeComponent();
        }

        private void label_key_press(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }

        
            if (e.KeyChar == '.' && (sender as TextBox).Text.Contains('.'))
            {
                e.Handled = true;
            }
        }

        private void check_button(object sender, KeyEventArgs e)
        {
            if (textBox1.Text.Length == 0)
            {
                button1.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Text = label_to_uah;
        }

        private void check_pln_checkbox(object sender, EventArgs e)
        {
            if (sender is CheckBox checkBox && checkBox.Checked == true)
            {
                to_uah = false;
                label1.Text = label_to_pln;
            } else
            {
                label1.Text = label_to_uah;
                to_uah = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                double value = double.Parse(textBox1.Text);
                double result;
                if (to_uah)
                {
                    result = value * coeficient;
                } else
                {
                    result = value / coeficient;
                }
                
                label2.Text = result.ToString();
            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
