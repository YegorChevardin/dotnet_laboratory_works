﻿using System.Text;

namespace Task02
{
    public static class Task02
    {
        static void Main()
        {
            Console.WriteLine("Введіть рядок:");
            string inputString = Console.ReadLine();

            try
            {
                Console.Write("Введіть бажану довжину рядка: ");
                int desiredLength = int.Parse(Console.ReadLine());

                string result = AdjustStringLength(inputString, desiredLength);
                Console.WriteLine($"Результат: {result}");
            }
            catch (FormatException)
            {
                Console.WriteLine("Помилка: Введено некоректне значення. Введіть ціле число.");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine($"Помилка: {ex.Message}");
            }

            Console.WriteLine("Натисніть Enter для завершення.");
            Console.ReadLine();
        }

        static string AdjustStringLength(string input, int desiredLength)
        {
            if (desiredLength < input.Length)
            {
                throw new ArgumentException("Бажана довжина рядка менше поточної довжини рядка.");
            }

            StringBuilder result = new StringBuilder(input);

            while (result.Length < desiredLength)
            {
                int spaceIndex = result.ToString().IndexOf(' ');

                if (spaceIndex == -1)
                {
                    spaceIndex = 0;
                }

                result.Insert(spaceIndex, ' ');
            }

            return result.ToString().Substring(0, desiredLength);
        }
    }
}