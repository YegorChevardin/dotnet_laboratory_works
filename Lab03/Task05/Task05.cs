﻿namespace Task05
{
    public static class Task05
    {
        static void Main()
        {
            Console.WriteLine("Введіть рядок:");
            string inputString = Console.ReadLine();

            int wordCount = CountWordsStartingWithUkrainianVowels(inputString);

            Console.WriteLine($"Кількість слів, що починаються на українські голосні літери: {wordCount}");

            Console.WriteLine("Натисніть Enter для завершення.");
            Console.ReadLine();
        }

        static bool IsUkrainianVowel(char letter)
        {
            char[] ukrainianVowels = { 'а', 'е', 'є', 'и', 'і', 'ї', 'о', 'у', 'ю', 'я' };
            return Array.Exists(ukrainianVowels, v => v == char.ToLower(letter));
        }

        static int CountWordsStartingWithUkrainianVowels(string input)
        {
            string[] words = input.Split(new char[] { ' ', '.', ',', ';', ':', '!', '?' },
                StringSplitOptions.RemoveEmptyEntries);
            int count = 0;

            foreach (string word in words)
            {
                if (word.Length > 0 && IsUkrainianVowel(word[0]))
                {
                    count++;
                }
            }

            return count;
        }
    }
}