﻿using System.Text.RegularExpressions;

namespace Task06
{
    public static class Task06
    {
        static void Main()
        {
            Console.WriteLine("Введіть текстовий рядок:");
            string inputString = Console.ReadLine();
            
            int wordCount = CountWords(inputString);
            Console.WriteLine($"1) Кількість слів у тексті: {wordCount}");
            
            string wordWithMostVowels = FindWordWithMostVowels(inputString);
            Console.WriteLine($"2) Слово з найбільшою кількістю голосних літер: {wordWithMostVowels}");
            
            string cleanedString = RemoveUnnecessarySpaces(inputString);
            Console.WriteLine($"3) Текст без непотрібних пробілів: {cleanedString}");

            Console.WriteLine("Натисніть Enter для завершення.");
            Console.ReadLine();
        }

        static int CountWords(string input)
        {
            string[] words = input.Split(new char[] { ' ', '.', ',', ';', ':', '!', '?' },
                StringSplitOptions.RemoveEmptyEntries);
            return words.Length;
        }

        static string FindWordWithMostVowels(string input)
        {
            string[] words = input.Split(new char[] { ' ', '.', ',', ';', ':', '!', '?' },
                StringSplitOptions.RemoveEmptyEntries);

            string wordWithMostVowels = "";
            int maxVowelCount = 0;

            foreach (string word in words)
            {
                int vowelCount = CountVowels(word);
                if (vowelCount > maxVowelCount)
                {
                    maxVowelCount = vowelCount;
                    wordWithMostVowels = word;
                }
            }

            return wordWithMostVowels;
        }

        static int CountVowels(string word)
        {
            char[] vowels =
                { 'а', 'е', 'є', 'и', 'і', 'ї', 'о', 'у', 'ю', 'я', 'А', 'Е', 'Є', 'И', 'І', 'Ї', 'О', 'У', 'Ю', 'Я' };
            return word.Count(c => vowels.Contains(c));
        }

        static string RemoveUnnecessarySpaces(string input)
        {
            return Regex.Replace(input, @"\s+", " ");
        }
    }
}