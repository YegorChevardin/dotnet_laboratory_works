﻿namespace Task03
{
    public static class Task03
    {
        static void Main()
        {
            Console.WriteLine("Введіть рядок:");
            string inputString = Console.ReadLine();

            List<int> positions = FindLetterCPositions(inputString, 'С');

            if (positions.Count > 0)
            {
                Console.WriteLine("Позиції входження букви 'С':");
                foreach (int position in positions)
                {
                    Console.WriteLine(position);
                }
            }
            else
            {
                Console.WriteLine("Буква 'С' не знайдена у введеному рядку.");
            }

            Console.WriteLine("Натисніть Enter для завершення.");
            Console.ReadLine();
        }

        static List<int> FindLetterCPositions(string input, char letter)
        {
            List<int> positions = new List<int>();

            for (int i = 0; i < input.Length; i++)
            {
                if (char.ToUpper(input[i]) == char.ToUpper(letter))
                {
                    positions.Add(i);
                }
            }

            return positions;
        }
    }
}