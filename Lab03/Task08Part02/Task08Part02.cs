﻿namespace Task08Part02
{
    public static class Task08Part02
    {
        static void Main()
        {
            Console.WriteLine("Введіть рядок:");
            string inputString = Console.ReadLine();

            int wordCount = CountWordsStartingWithUkrainianVowels(inputString);
            
            string binaryFileName = "result.bin";
            SaveResultToBinaryFile(wordCount, binaryFileName);
            Console.WriteLine($"Результат збережено у файлі: {binaryFileName}");
            
            int readResult = ReadResultFromBinaryFile(binaryFileName);
            Console.WriteLine($"Результат з файлу: {readResult}");
            
            if (wordCount == readResult)
            {
                Console.WriteLine("Результати співпадають. Обробка пройшла успішно.");
            }
            else
            {
                Console.WriteLine("Помилка: Результати не співпадають.");
            }

            Console.WriteLine("Натисніть Enter для завершення.");
            Console.ReadLine();
        }

        static int CountWordsStartingWithUkrainianVowels(string input)
        {
            string[] words = input.Split(new char[] { ' ', '.', ',', ';', ':', '!', '?' },
                StringSplitOptions.RemoveEmptyEntries);
            int count = 0;

            foreach (string word in words)
            {
                if (word.Length > 0 && IsUkrainianVowel(word[0]))
                {
                    count++;
                }
            }

            return count;
        }

        static bool IsUkrainianVowel(char letter)
        {
            char[] ukrainianVowels = { 'а', 'е', 'є', 'и', 'і', 'ї', 'о', 'у', 'ю', 'я' };
            return Array.Exists(ukrainianVowels, v => v == char.ToLower(letter));
        }

        static void SaveResultToBinaryFile(int result, string fileName)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(fileName, FileMode.Create)))
            {
                writer.Write(result);
            }
        }

        static int ReadResultFromBinaryFile(string fileName)
        {
            using (BinaryReader reader = new BinaryReader(File.Open(fileName, FileMode.Open)))
            {
                return reader.ReadInt32();
            }
        }
    }
}