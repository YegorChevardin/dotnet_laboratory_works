﻿namespace Task04
{
    public static class Task04
    {
        static void Main()
        {
            Console.WriteLine("Введіть рядок:");
            string inputString = Console.ReadLine();

            string resultString = ReplaceSpacesWithDoubleSpaces(inputString);

            Console.WriteLine($"Результат: {resultString}");

            Console.WriteLine("Натисніть Enter для завершення.");
            Console.ReadLine();
        }

        static string ReplaceSpacesWithDoubleSpaces(string input)
        {
            return input.Replace(" ", "  ");
        }
    }
}