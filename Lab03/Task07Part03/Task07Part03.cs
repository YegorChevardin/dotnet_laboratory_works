﻿namespace Task07Part
{
    public static class Task07Part03
    {
        static void Main()
        {
            string inputFile = "input.txt";
            string outputFile = "output.txt";

            try
            {
                string inputString = File.ReadAllText(inputFile);

                List<int> positions = FindLetterCPositions(inputString, 'С');

                using (StreamWriter writer = new StreamWriter(outputFile))
                {
                    if (positions.Count > 0)
                    {
                        writer.WriteLine("Позиції входження букви 'С':");
                        foreach (int position in positions)
                        {
                            writer.WriteLine(position);
                        }
                    }
                    else
                    {
                        writer.WriteLine("Буква 'С' не знайдена у введеному рядку.");
                    }

                    Console.WriteLine("Результати записані в файл: " + outputFile);
                }
            }
            catch (IOException)
            {
                Console.WriteLine("Помилка: Не вдалося зчитати або записати файли.");
            }

            Console.WriteLine("Натисніть Enter для завершення.");
            Console.ReadLine();
        }

        static List<int> FindLetterCPositions(string input, char letter)
        {
            List<int> positions = new List<int>();

            for (int i = 0; i < input.Length; i++)
            {
                if (char.ToUpper(input[i]) == char.ToUpper(letter))
                {
                    positions.Add(i);
                }
            }

            return positions;
        }
    }
}