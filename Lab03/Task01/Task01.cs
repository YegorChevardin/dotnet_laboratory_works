﻿namespace Task01
{
    public static class Task01
    {
        static void Main()
        {
            Console.WriteLine("Введіть рядок:");
            string inputString = Console.ReadLine();

            try
            {
                Console.Write("Введіть початковий індекс слова: ");
                int startingWordIndex =
                    int.Parse(Console.ReadLine());

                Console.Write("Введіть кількість слів: ");
                int wordCount = int.Parse(Console.ReadLine());

                string result = ExtractWords(inputString, startingWordIndex, wordCount);
                Console.WriteLine($"Результат: {result}");
            }
            catch (FormatException)
            {
                Console.WriteLine("Помилка: Введено некоректне значення. Введіть ціле число.");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine($"Помилка: {ex.Message}");
            }

            Console.WriteLine("Натисніть Enter для завершення.");
            Console.ReadLine();
        }

        static string ExtractWords(string input, int startingWordIndex, int wordCount)
        {
            string[] words = input.Split(new char[] { ' ', '.', ',', ';', ':', '!', '?' },
                StringSplitOptions.RemoveEmptyEntries);

            if (startingWordIndex >= 0 && startingWordIndex < words.Length)
            {
                int endingWordIndex = startingWordIndex + wordCount - 1;

                if (endingWordIndex < words.Length)
                {
                    string[] selectedWords = words.Skip(startingWordIndex).Take(wordCount).ToArray();
                    string result = string.Join(" ", selectedWords);
                    return result;
                }
                else
                {
                    throw new ArgumentException("Недостатньо слів у рядку, щоб виділити задану кількість слів.");
                }
            }
            else
            {
                throw new ArgumentException("Початковий індекс знаходиться поза межами масиву слів.");
            }
        }
    }
}