﻿using System.Text;

namespace Task08Part01
{
    public static class Task08Part01
    {
        static void Main()
        {
            Console.WriteLine("Введіть рядок:");
            string inputString = Console.ReadLine();

            string resultString = ReplaceSpacesWithDoubleSpaces(inputString);
            
            string binaryFileName = "result.bin";
            SaveResultToBinaryFile(resultString, binaryFileName);
            Console.WriteLine($"Результат збережено у файлі: {binaryFileName}");
            
            string readResult = ReadResultFromBinaryFile(binaryFileName);
            Console.WriteLine($"Результат з файлу: {readResult}");
            
            if (resultString == readResult)
            {
                Console.WriteLine("Результати співпадають. Обробка пройшла успішно.");
            }
            else
            {
                Console.WriteLine("Помилка: Результати не співпадають.");
            }

            Console.WriteLine("Натисніть Enter для завершення.");
            Console.ReadLine();
        }

        static string ReplaceSpacesWithDoubleSpaces(string input)
        {
            return input.Replace(" ", "  ");
        }

        static void SaveResultToBinaryFile(string result, string fileName)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(fileName, FileMode.Create)))
            {
                writer.Write(Encoding.UTF8.GetBytes(result));
            }
        }

        static string ReadResultFromBinaryFile(string fileName)
        {
            using (BinaryReader reader = new BinaryReader(File.Open(fileName, FileMode.Open)))
            {
                byte[] bytes = reader.ReadBytes((int)reader.BaseStream.Length);
                return Encoding.UTF8.GetString(bytes);
            }
        }
    }
}