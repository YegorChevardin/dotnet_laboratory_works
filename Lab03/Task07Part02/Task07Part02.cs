﻿using System.Text;

namespace Task07Part02
{
    public static class Task07Part02
    {
        static void Main()
        {
            string inputFile = "input.txt";
            string outputFile = "output.txt";

            try
            {
                StreamReader streamReader = new StreamReader(inputFile);
                string inputString = streamReader.ReadLine();

                Console.Write("Введіть бажану довжину рядка: ");
                int desiredLength = int.Parse(streamReader.ReadLine());

                string result = AdjustStringLength(inputString, desiredLength);
                
                File.WriteAllText(outputFile, result);

                Console.WriteLine("Результати записані в файл: " + outputFile);
            }
            catch (FormatException)
            {
                Console.WriteLine("Помилка: Некоректні дані в файлі вводу.");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine($"Помилка: {ex.Message}");
            }
            catch (IOException)
            {
                Console.WriteLine("Помилка: Не вдалося зчитати або записати файли.");
            }

            Console.WriteLine("Натисніть Enter для завершення.");
            Console.ReadLine();
        }

        static string AdjustStringLength(string input, int desiredLength)
        {
            if (desiredLength < input.Length)
            {
                throw new ArgumentException("Бажана довжина рядка менше поточної довжини рядка.");
            }

            StringBuilder result = new StringBuilder(input);

            while (result.Length < desiredLength)
            {
                int spaceIndex = result.ToString().IndexOf(' ');

                if (spaceIndex == -1)
                {
                    spaceIndex = 0;
                }

                result.Insert(spaceIndex, ' ');
            }
            return result.ToString().Substring(0, desiredLength);
        }
    }
}