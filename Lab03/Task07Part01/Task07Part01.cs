﻿namespace Task07Part01
{
    public static class Task07Part01
    {
        static void Main()
        {
            string inputFile = "input.txt";
            string outputFile = "output.txt";

            try
            {
                string inputString = File.ReadAllText(inputFile);
                string[] inputLines = File.ReadAllLines(inputFile);

                int startingWordIndex = 0;
                int wordCount = 0;

                if (inputLines.Length >= 3)
                {
                    startingWordIndex = int.Parse(inputLines[1]);
                    wordCount = int.Parse(inputLines[2]);
                }
                else
                {
                    Console.WriteLine("Некоректний формат вхідного файлу.");
                    return;
                }

                string result = ExtractWords(inputString, startingWordIndex, wordCount);

                // Write the result to the output file
                File.WriteAllText(outputFile, result);

                Console.WriteLine("Результати записані в файл: " + outputFile);
            }
            catch (FormatException)
            {
                Console.WriteLine("Помилка: Некоректні дані в файлі вводу.");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine($"Помилка: {ex.Message}");
            }
            catch (IOException)
            {
                Console.WriteLine("Помилка: Не вдалося зчитати або записати файли.");
            }

            Console.WriteLine("Натисніть Enter для завершення.");
            Console.ReadLine();
        }

        static string ExtractWords(string input, int startingWordIndex, int wordCount)
        {
            string[] words = input.Split(new char[] { ' ', '.', ',', ';', ':', '!', '?' },
                StringSplitOptions.RemoveEmptyEntries);

            if (startingWordIndex >= 0 && startingWordIndex < words.Length)
            {
                int endingWordIndex = startingWordIndex + wordCount - 1;

                if (endingWordIndex < words.Length)
                {
                    string[] selectedWords = words.Skip(startingWordIndex).Take(wordCount).ToArray();
                    string result = string.Join(" ", selectedWords);
                    return result;
                }
                else
                {
                    throw new ArgumentException("Недостатньо слів у рядку, щоб виділити задану кількість слів.");
                }
            }
            else
            {
                throw new ArgumentException("Початковий індекс знаходиться поза межами масиву слів.");
            }
        }
    }
}