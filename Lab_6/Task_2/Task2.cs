﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_2
{
    public partial class Task2 : Form
    {
        string[] cities = new string[] { "Будапешт", "Дебрецен", "Мішкольц", "Сегед", "Пешт", "Гьор", "Ньїрегихаза" };
        private ComboBox comboBox;
        private ListBox listBox;
        public Task2()
        {
            comboBox = new ComboBox();
            listBox = new ListBox();
            InitializeComponent();
        }

        private void Task2_Load(object sender, EventArgs e)
        {
            comboBox.Location = new System.Drawing.Point(20, 20);
            comboBox.Width = 150;
            comboBox.Items.AddRange(cities);

            listBox.Location = new System.Drawing.Point(20, 60);
            listBox.Width = 150;
            listBox.Height = 120;
            listBox.Items.AddRange(cities);

            Controls.Add(comboBox);
            Controls.Add(listBox);
        }
    }
}
