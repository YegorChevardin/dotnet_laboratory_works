﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task02
{
    public partial class Form1 : Form
    {
        private Label label1 = new Label();
        private Label label2 = new Label();
        private Label label3 = new Label();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Text = "First label text example";
            label2.Text = "Second label text example";
            label3.Text = "Third label text example";

            label1.Font = new Font("Arial", 12);
            label2.Font = new Font("Arial", 13);
            label3.Font = new Font("Arial", 17);

            label1.ForeColor = Color.Gray;
            label2.ForeColor = Color.Brown;
            label3.ForeColor = Color.Yellow;

            label1.BackColor = Color.Red;
            label2.BackColor = Color.Purple;
            label3.BackColor = Color.Green;

            label1.Font = new Font(label1.Font, FontStyle.Bold | FontStyle.Underline);
            label2.Font = new Font(label2.Font, FontStyle.Italic | FontStyle.Underline);
            label3.Font = new Font(label3.Font, FontStyle.Bold | FontStyle.Italic | FontStyle.Underline);

            label1.TextAlign = ContentAlignment.MiddleLeft;
            label2.TextAlign = ContentAlignment.MiddleRight;
            label3.TextAlign = ContentAlignment.MiddleLeft;

            label1.Location = new System.Drawing.Point(63, 186);
            label2.Location = new System.Drawing.Point(254, 186);
            label3.Location = new System.Drawing.Point(458, 186);

            this.Controls.Add(label1);
            this.Controls.Add(label2);
            this.Controls.Add(label3);
        }
    }
}
