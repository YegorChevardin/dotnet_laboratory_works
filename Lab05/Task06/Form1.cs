﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task06
{
    public partial class Form1 : Form
    {
        private PictureBox pictureBox1 = new PictureBox();
        private PictureBox pictureBox2 = new PictureBox();
        private PictureBox pictureBox3 = new PictureBox();
        private PictureBox pictureBox4 = new PictureBox();
        private PictureBox pictureBox5 = new PictureBox();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox1.Image = System.Drawing.Bitmap.FromFile("C:\\Users\\Yehor\\Desktop\\dotnet_laboratory_works\\Lab05\\Task06\\R.jpg");
            pictureBox2.Image = System.Drawing.Bitmap.FromFile("C:\\Users\\Yehor\\Desktop\\dotnet_laboratory_works\\Lab05\\Task06\\R.jpg");
            pictureBox3.Image = System.Drawing.Bitmap.FromFile("C:\\Users\\Yehor\\Desktop\\dotnet_laboratory_works\\Lab05\\Task06\\R.jpg");
            pictureBox4.Image = System.Drawing.Bitmap.FromFile("C:\\Users\\Yehor\\Desktop\\dotnet_laboratory_works\\Lab05\\Task06\\R.jpg");
            pictureBox5.Image = System.Drawing.Bitmap.FromFile("C:\\Users\\Yehor\\Desktop\\dotnet_laboratory_works\\Lab05\\Task06\\R.jpg");

            pictureBox1.Size = new System.Drawing.Size(100, 50);
            pictureBox2.Size = new System.Drawing.Size(100, 50);
            pictureBox3.Size = new System.Drawing.Size(171, 124);
            pictureBox4.Size = new System.Drawing.Size(153, 102);
            pictureBox5.Size = new System.Drawing.Size(171, 124);

            pictureBox1.Location = new System.Drawing.Point(45, 244);
            pictureBox2.Location = new System.Drawing.Point(151, 244);
            pictureBox3.Location = new System.Drawing.Point(257, 170);
            pictureBox4.Location = new System.Drawing.Point(434, 192);
            pictureBox5.Location = new System.Drawing.Point(593, 170);

            Controls.Add(pictureBox1);
            Controls.Add(pictureBox2);
            Controls.Add(pictureBox3);
            Controls.Add(pictureBox4);
            Controls.Add(pictureBox5);
        }
    }
}
