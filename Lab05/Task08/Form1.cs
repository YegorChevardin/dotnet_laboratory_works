﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task08
{
    public partial class Form1 : Form
    {
        private TableLayoutPanel tablelayoutpanel1 = new TableLayoutPanel();
        private Button button1 = new Button();
        private Button button2 = new Button();
        private Button button3 = new Button();
        private Button button4 = new Button();
        private Button button5 = new Button();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tablelayoutpanel1.RowStyles.Add(new RowStyle(SizeType.AutoSize));
            tablelayoutpanel1.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));

            tablelayoutpanel1.Controls.Add(button1, 0, 0);
            tablelayoutpanel1.Controls.Add(button2, 1, 1);
            tablelayoutpanel1.Controls.Add(button3, 0, 2);
            tablelayoutpanel1.Controls.Add(button4, 2, 0);
            tablelayoutpanel1.Controls.Add(button5, 0, 1);

            tablelayoutpanel1.Visible = true;

            button1.Text = "button1";
            button2.Text = "button2";
            button3.Text = "button3";
            button4.Text = "button4";
            button5.Text = "button5";

            button1.Margin = new Padding(3, 5, 3, 3);
            button2.Margin = new Padding(3, 1, 3, 3);
            button3.Margin = new Padding(3, 5, 3, 3);
            button4.Margin = new Padding(3, 5, 3, 3);
            button5.Margin = new Padding(10, 5, 3, 3);

            tablelayoutpanel1.Size = new System.Drawing.Size(799, 453);
            button1.Size = new System.Drawing.Size(70, 76);
            button2.Size = new System.Drawing.Size(70, 76);
            button3.Size = new System.Drawing.Size(70, 76);
            button4.Size = new System.Drawing.Size(70, 76);
            button5.Size = new System.Drawing.Size(70, 76);

            this.Controls.Add(tablelayoutpanel1);
        }
    }
}
