﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task04
{
    public partial class Form1 : Form
    {
        private TextBox textbox1 = new TextBox();
        private TextBox textbox2 = new TextBox();
        private TextBox textbox3 = new TextBox();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textbox1.Text = "First label text example";
            textbox2.Text = "Second label text example";
            textbox3.Text = "Third label text example";

            textbox1.Font = new Font("Arial", 12);
            textbox2.Font = new Font("Arial", 13);
            textbox3.Font = new Font("Arial", 17);

            textbox1.ForeColor = Color.Gray;
            textbox2.ForeColor = Color.Brown;
            textbox3.ForeColor = Color.Yellow;

            textbox1.BackColor = Color.Red;
            textbox2.BackColor = Color.Purple;
            textbox3.BackColor = Color.Green;

            textbox1.Font = new Font(textbox1.Font, FontStyle.Bold | FontStyle.Underline);
            textbox2.Font = new Font(textbox2.Font, FontStyle.Italic | FontStyle.Underline);
            textbox3.Font = new Font(textbox3.Font, FontStyle.Bold | FontStyle.Italic | FontStyle.Underline);

            textbox1.TextAlign = HorizontalAlignment.Left;
            textbox2.TextAlign = HorizontalAlignment.Right;
            textbox3.TextAlign = HorizontalAlignment.Left;

            textbox1.Location = new System.Drawing.Point(63, 186);
            textbox2.Location = new System.Drawing.Point(254, 186);
            textbox3.Location = new System.Drawing.Point(458, 186);

            this.Controls.Add(textbox1);
            this.Controls.Add(textbox2);
            this.Controls.Add(textbox3);
        }
    }
}
