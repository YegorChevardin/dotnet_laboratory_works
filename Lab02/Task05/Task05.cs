﻿namespace Task05
{
    public static class Task05
    {
        public static void Main(string[] args)
        {
            int matrixSize = 9; // Розмір матриці
            int[,] matrix = new int[matrixSize, matrixSize];

            int cellValue = 1; // Початне значення для заповнення матриці
            int currentRow = matrixSize / 2; // Поточний рядок (початок середини матриці)
            int currentCol = matrixSize / 2; // Поточна колонка (початок середини матриці)
            int currentStep = 1; // Поточний крок

            // Заповнюємо матрицю по спіралі
            while (cellValue <= matrixSize * matrixSize)
            {
                // Рухаємось вниз
                for (int i = 0; i < currentStep; i++)
                {
                    if (currentRow >= 0 && currentRow < matrixSize && currentCol >= 0 && currentCol < matrixSize)
                        matrix[currentRow, currentCol] = cellValue++;
                    currentRow++;
                }

                // Рухаємось вліво
                for (int i = 0; i < currentStep; i++)
                {
                    if (currentRow >= 0 && currentRow < matrixSize && currentCol >= 0 && currentCol < matrixSize)
                        matrix[currentRow, currentCol] = cellValue++;
                    currentCol--;
                }

                currentStep++; // Збільшуємо поточний крок

                // Рухаємось вгору
                for (int i = 0; i < currentStep; i++)
                {
                    if (currentRow >= 0 && currentRow < matrixSize && currentCol >= 0 && currentCol < matrixSize)
                        matrix[currentRow, currentCol] = cellValue++;
                    currentRow--;
                }

                // Рухаємось вправо
                for (int i = 0; i < currentStep; i++)
                {
                    if (currentRow >= 0 && currentRow < matrixSize && currentCol >= 0 && currentCol < matrixSize)
                        matrix[currentRow, currentCol] = cellValue++;
                    currentCol++;
                }

                currentStep++; // Збільшуємо поточний крок
            }

            // Виводимо матрицю
            Console.WriteLine("Сформована матриця:");
            for (int i = 0; i < matrixSize; i++)
            {
                for (int j = 0; j < matrixSize; j++)
                {
                    Console.Write($"{matrix[i, j],3} ");
                }
                Console.WriteLine();
            }
        }
    }
}