﻿using System;
using System.Collections;

namespace Task04
{
    public static class Task04
    {
        public static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Введіть кількість елементів масиву: ");
            int arrayLength = int.Parse(Console.ReadLine());

            ArrayList arrayList = new ArrayList();
            int sequenceCount = 0;
            int currentSequenceLength = 0;

            Random random = new Random();

            for (int i = 0; i < arrayLength; i++)
            {
                arrayList.Add(random.Next(-50, 51));
            }

            Console.WriteLine("Масив випадкових чисел:");
            Console.ForegroundColor = ConsoleColor.Yellow;
            for (int i = 0; i < arrayList.Count; i++)
            {
                Console.Write($"{(int)arrayList[i],3} ");
                if ((i + 1) % 10 == 0)
                    Console.WriteLine();
            }
            Console.WriteLine();

            for (int i = 0; i < arrayList.Count; i++)
            {
                if ((int)arrayList[i] >= 0)
                {
                    currentSequenceLength++;
                    if (currentSequenceLength > 7)
                    {
                        sequenceCount++;
                    }
                }
                else
                {
                    currentSequenceLength = 0;
                }
            }
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"Кількість безперервних послідовностей позитивних чисел завдовжки більше 7: {sequenceCount}");
        }
    }
}