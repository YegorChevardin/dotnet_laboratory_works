﻿namespace Task09
{
    public static class Task09
    {
        public static void Main()
        {
            Console.Write("Введіть кількість рядків матриці: ");
            if (int.TryParse(Console.ReadLine(), out int numRows) && numRows > 0)
            {
                Console.Write("Введіть кількість стовпців матриці: ");
                if (int.TryParse(Console.ReadLine(), out int numCols) && numCols > 0)
                {
                    int[][] matrix = GenerateRandomMatrix(numRows, numCols);

                    Console.WriteLine("\nМатриця:");
                    PrintMatrix(matrix);

                    int[] rowSumsWithNegatives = CalculateRowSumsWithNegativeElements(matrix);

                    Console.WriteLine("\nСуми елементів у рядках із хоча б одним від'ємним елементом:");
                    for (int i = 0; i < rowSumsWithNegatives.Length; i++)
                    {
                        Console.WriteLine($"Рядок {i + 1}: {rowSumsWithNegatives[i]}");
                    }
                }
                else
                {
                    Console.WriteLine("Неправильний ввід кількості стовпців.");
                }
            }
            else
            {
                Console.WriteLine("Неправильний ввід кількості рядків.");
            }
        }

        static int[][] GenerateRandomMatrix(int numRows, int numCols)
        {
            int[][] matrix = new int[numRows][];
            Random random = new Random();

            for (int i = 0; i < numRows; i++)
            {
                matrix[i] = new int[numCols];
                for (int j = 0; j < numCols; j++)
                {
                    matrix[i][j] = random.Next(-10, 11);
                }
            }

            return matrix;
        }

        static void PrintMatrix(int[][] matrix)
        {
            foreach (var row in matrix)
            {
                Console.WriteLine(string.Join(" ", row.Select(element => $"{element,3}")));
            }
        }

        static int[] CalculateRowSumsWithNegativeElements(int[][] matrix)
        {
            int[] sums = new int[matrix.Length];
            for (int i = 0; i < matrix.Length; i++)
            {
                if (matrix[i].Any(element => element < 0))
                {
                    sums[i] = matrix[i].Sum();
                }
            }

            return sums;
        }
    }
}