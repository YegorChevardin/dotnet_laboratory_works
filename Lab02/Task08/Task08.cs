﻿using System;

namespace Task08
{
    public static class Task08
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Вкажіть розмір матриці: ");
            int matrixSize = int.Parse(Console.ReadLine()); // Розмір матриці
            int[][] matrix = new int[matrixSize][];

            for (int i = 0; i < matrixSize; i++)
            {
                matrix[i] = new int[matrixSize];
            }

            int cellValue = 1; // Початкове значення для заповнення матриці
            int currentRow = matrixSize / 2; // Поточний рядок (початок середини матриці)
            int currentCol = matrixSize / 2; // Поточна колонка (початок середини матриці)
            int currentStep = 1; // Поточний крок

            // Заповнюємо матрицю по спіралі
            while (cellValue <= matrixSize * matrixSize)
            {
                // Рухаємось вниз
                for (int i = 0; i < currentStep; i++)
                {
                    if (currentRow >= 0 && currentRow < matrixSize && currentCol >= 0 && currentCol < matrixSize)
                        matrix[currentRow][currentCol] = cellValue++;
                    currentRow++;
                }

                // Рухаємось вліво
                for (int i = 0; i < currentStep; i++)
                {
                    if (currentRow >= 0 && currentRow < matrixSize && currentCol >= 0 && currentCol < matrixSize)
                        matrix[currentRow][currentCol] = cellValue++;
                    currentCol--;
                }

                currentStep++; // Збільшуємо поточний крок

                // Рухаємось вгору
                for (int i = 0; i < currentStep; i++)
                {
                    if (currentRow >= 0 && currentRow < matrixSize && currentCol >= 0 && currentCol < matrixSize)
                        matrix[currentRow][currentCol] = cellValue++;
                    currentRow--;
                }

                // Рухаємось вправо
                for (int i = 0; i < currentStep; i++)
                {
                    if (currentRow >= 0 && currentRow < matrixSize && currentCol >= 0 && currentCol < matrixSize)
                        matrix[currentRow][currentCol] = cellValue++;
                    currentCol++;
                }

                currentStep++; // Збільшуємо поточний крок
            }

            // Виводимо матрицю з кольоровим текстом і символами
            Console.WriteLine("Сформована матриця:");

            ConsoleColor[] colors = { ConsoleColor.Red, ConsoleColor.DarkYellow, ConsoleColor.Yellow, ConsoleColor.Green, ConsoleColor.Blue, ConsoleColor.Magenta };
            int colorIndex = 0;

            for (int i = 0; i < matrixSize; i++)
            {
                for (int j = 0; j < matrixSize; j++)
                {
                    Console.ForegroundColor = colors[colorIndex];
                    Console.Write($" {matrix[i][j],3} ");
                    colorIndex = (colorIndex + 1) % colors.Length;
                }

                Console.WriteLine();
            }

            Console.ResetColor(); // Скидаємо колір консолі
        }
    }
}
