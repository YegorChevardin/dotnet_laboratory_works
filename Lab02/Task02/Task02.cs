﻿namespace Task02
{
    public static class Task02
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Введіть кількість елементів масиву: ");
            int arrayLength = int.Parse(Console.ReadLine());
            
            int[] array = new int[arrayLength];
            int sequenceCount = 0;
            int currentSequenceLength = 0;

            Random random = new Random();
            
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = random.Next(-50, 51);
            }
            
            Console.WriteLine("Масив випадкових чисел:");
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write($"{array[i],3} ");
                if ((i + 1) % 10 == 0)
                    Console.WriteLine();
            }
            Console.WriteLine();
            
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] >= 0)
                {
                    currentSequenceLength++;
                    if (currentSequenceLength > 7)
                    {
                        sequenceCount++;
                    }
                }
                else
                {
                    currentSequenceLength = 0;
                }
            }

            Console.WriteLine($"Кількість безперервних послідовностей позитивних чисел завдовжки більше 7: {sequenceCount}");
        }
    }
}