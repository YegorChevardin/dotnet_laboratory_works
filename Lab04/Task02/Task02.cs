﻿using Task02.dto;
using Task02.dto.impl;
using Task02.dto.impl.extension;
using Task02.list.impl;

namespace Task02;

public static class Task02
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Введіть кількість старих книжок: ");
        int amountOfOldBooks = int.Parse(Console.ReadLine());
        Console.WriteLine("Введіть кількість електронних книжок: ");
        int amountOfEbooks = int.Parse(Console.ReadLine());
        Console.WriteLine("Введіть кількість газет: ");
        int amountOfNewsPapers = int.Parse(Console.ReadLine());

        Reader[] oldBooks = GenerateOldBooks(amountOfOldBooks);
        Reader[] eBooks = GenerateEBooks(amountOfEbooks);
        Reader[] newsPapers = GenerateNewsPapers(amountOfNewsPapers);

        Reader[] allTypes = oldBooks.Concat(eBooks).Concat(newsPapers).ToArray();
        ReaderList readerList = new ReaderList(allTypes);

        Console.WriteLine("Масив с елементами: ");
        readerList.Show();

        Console.WriteLine("Читання всіх елементів: ");
        readerList.ReadAll();

        Console.WriteLine("Боргування всіх елементів: ");
        readerList.BorrowAll();

        Console.WriteLine("Повернення всіх елементів: ");
        readerList.ReturnAll();
        
        Console.WriteLine("Виведення за автором Author1");
        readerList.showByAuthor("Author1");
            
        Console.WriteLine();
            
        Console.WriteLine("Виведення книг пілся 2010 року");
        readerList.showReadersAfterYear(2010);
            
        Console.WriteLine();
            
        Console.WriteLine("Виведення книг видавнитства Publisher2");
        readerList.ShowReaderByPublisher("Publisher2");
            
        Console.WriteLine();
            
        Console.WriteLine("Додавання нової книги (Author = Yehor)");
        readerList.Update(0, new Book("Yehor", "Some", "Somesome", 2004, 100));
            
        Console.WriteLine("Виведення за автором Yehor");
        readerList.showByAuthor("Yehor");
    }

    static Reader[] GenerateOldBooks(int count)
    {
        string[] authors = { "Author1", "Author2", "Author3", "Author4", "Author5" };
        string[] titles = { "Title1", "Title2", "Title3", "Title4", "Title5" };
        string[] publishers = { "PublisherA", "PublisherB", "PublisherC", "PublisherD", "PublisherE" };

        Random random = new Random();
        List<Reader> randomBooks = new List<Reader>();

        for (int i = 0; i < count; i++)
        {
            string randomAuthor = authors[random.Next(authors.Length)];
            string randomTitle = titles[random.Next(titles.Length)];
            string randomPublisher = publishers[random.Next(publishers.Length)];
            int randomYear = random.Next(2000, 2023);
            int randomPageCount = random.Next(100, 500);
            int randomReprintCount = random.Next(0, 1000);

            randomBooks.Add(new OldBook(randomAuthor, randomTitle, randomPublisher, randomYear, randomPageCount,
                randomReprintCount));
        }

        return randomBooks.ToArray();
    }

    static Reader[] GenerateEBooks(int count)
    {
        string[] authors = { "Author1", "Author2", "Author3", "Author4", "Author5" };
        string[] titles = { "Title1", "Title2", "Title3", "Title4", "Title5" };
        string[] publishers = { "PublisherA", "PublisherB", "PublisherC", "PublisherD", "PublisherE" };
        string[] formats = { "pdf", "epub", "txt", "word" };

        Random random = new Random();
        List<Reader> randomBooks = new List<Reader>();

        for (int i = 0; i < count; i++)
        {
            string randomAuthor = authors[random.Next(authors.Length)];
            string randomTitle = titles[random.Next(titles.Length)];
            string randomPublisher = publishers[random.Next(publishers.Length)];
            int randomYear = random.Next(2000, 2023);
            int randomPageCount = random.Next(100, 500);
            string randomFormat = formats[random.Next(formats.Length)];

            randomBooks.Add(new EBook(randomAuthor, randomTitle, randomPublisher, randomYear, randomPageCount,
                randomFormat));
        }

        return randomBooks.ToArray();
    }

    static Reader[] GenerateNewsPapers(int count)
    {
        string[] authors = { "Author1", "Author2", "Author3", "Author4", "Author5" };
        string[] publishers = { "PublisherA", "PublisherB", "PublisherC", "PublisherD", "PublisherE" };
        string[] types = { "Sport", "News", "Daily news", "Information", "Articles" };

        Random random = new Random();
        List<Reader> randomBooks = new List<Reader>();

        for (int i = 0; i < count; i++)
        {
            string randomAuthor = authors[random.Next(authors.Length)];
            string randomPublisher = publishers[random.Next(publishers.Length)];
            string randomTypes = types[random.Next(types.Length)];
            int randomPageCount = random.Next(100, 500);
            int randomYear = random.Next(2000, 2023);

            randomBooks.Add(new Newspaper(randomTypes, randomPageCount, randomAuthor, randomPublisher, randomYear));
        }

        return randomBooks.ToArray();
    }
}