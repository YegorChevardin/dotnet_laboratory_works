namespace Task02.dto.impl;

public class Book : Reader
{
    private string _title;

    public Book(string author, string title, string publisher, int year, int pageCount) : base(pageCount, author,
        publisher, year)
    {
        _title = title;
    }

    private void ShowBook()
    {
        Console.WriteLine("Інформація про книгу:");
        Console.WriteLine($"Автор: {base.Author}");
        Console.WriteLine($"Назва: {_title}");
        Console.WriteLine($"Видавництво: {base.Publisher}");
        Console.WriteLine($"Рік видання: {base.Year}");
        Console.WriteLine($"Кількість сторінок: {PagesCount}");
    }

    public override void Read()
    {
        Console.WriteLine("Book is being read....");
        ShowBook();
    }

    public override void Show()
    {
        ShowBook();
    }

    public string Title
    {
        get { return _title; }
        set { _title = value; }
    }
}