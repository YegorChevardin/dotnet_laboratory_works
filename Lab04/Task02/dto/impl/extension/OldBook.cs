namespace Task02.dto.impl.extension;

public class OldBook : Book
{
    private int _reprintCount;

    public OldBook(string author, string title, string publisher, int year, int pageCount, int reprintCount) : base(
        author, title,
        publisher, year, pageCount)
    {
        _reprintCount = reprintCount;
    }

    public override void Read()
    {
        Console.WriteLine("Reading VERY OLD book");
        base.Read();
    }

    public int ReprintCount
    {
        get { return _reprintCount; }
        set { _reprintCount = value; }
    }
}