namespace Task02.list;

public interface AddReadUpdateDelete<T> : AddReadDelete<T>
{
    void Update(int index, T element);

    void Update(T oldObject, T newObject);
}