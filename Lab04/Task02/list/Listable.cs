namespace Task02.list;

public interface Listable<T> : AddReadUpdateDelete<T>
{
    bool hasNext();

    T next();
}