namespace Task02.list;

public abstract class AbstractList<T> : Listable<T>
{
    protected List<T> _elements = new List<T>();

    private int _currentIndex = 0;

    protected AbstractList()
    {
    }
    
    public T Read(int index)
    {
        return _elements[index];
    }

    public void Add(T elememt)
    {
        _elements.Add(elememt);
    }

    public void Delete(int index)
    {
        _elements.RemoveAt(index);
    }

    public void Delete(T element)
    {
        _elements.Remove(element);
    }

    public void Update(int index, T element)
    {
        _elements[index] = element;
    }

    public void Update(T oldObject, T newObject)
    {
        int index = _elements.IndexOf(oldObject);
        _elements[index] = newObject;
    }

    public bool hasNext()
    {
        try
        {
            T element = _elements[_currentIndex + 1];
            return true;
        }
        catch (IndexOutOfRangeException e)
        {
            return false;
        }
        catch (ArgumentOutOfRangeException e)
        {
            _currentIndex = 0;
            return false;
        }
    }

    public T next()
    {
        return _elements[_currentIndex++];
    }

    public int CurrentIndex
    {
        set { _currentIndex = value;}
        get { return _currentIndex; }
    }
}