namespace Task02.list;

public interface AddReadDelete<T>
{
    T Read(int index);
    
    void Add(T elememt);

    void Delete(int index);

    void Delete(T element);
}