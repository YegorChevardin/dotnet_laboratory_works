using Task02.dto;

namespace Task02.list.impl;

public class Library : AbstractList<Reader>
{
    public Library()
    {
    }

    public void ReadAll()
    {
        while (hasNext())
        {
            next().Read();
        }
    }

    public void BorrowAll()
    {
        while (hasNext())
        {
            next().Borrow();
        }
    }

    public void ReturnAll()
    {
        while (hasNext())
        {
            next().Return();
        }
    }

    public void Show()
    {
        Console.WriteLine("-----------");
        while (hasNext())
        {
            next().Show();
            Console.WriteLine();
        }

        Console.WriteLine("-----------");
    }

    private Reader[] ReaderByAuthor(string author)
    {
        return _elements.FindAll(reader => reader.Author.Name + " " + reader.Author.SecondName == author).ToArray();
    }

    public void showByAuthor(string author)
    {
        Reader[] readers = ReaderByAuthor(author);

        Console.WriteLine("------------");
        foreach (var reader in readers)
        {
            reader.Show();
            Console.WriteLine("");
        }

        Console.WriteLine("------------");
    }

    public Reader[] FindReadersAfterYear(int year)
    {
        return FindProperReadersAfterYear(year);
    }

    public void showReadersAfterYear(int year)
    {
        Reader[] readers = FindProperReadersAfterYear(year);
        Console.WriteLine("------------");
        foreach (var reader in readers)
        {
            reader.Show();
            Console.WriteLine("");
        }

        Console.WriteLine("------------");
    }

    private Reader[] FindProperReadersAfterYear(int year)
    {
        return _elements.FindAll(reader => reader.Year > year).ToArray();
    }

    public Reader[] FindReaderByPublisher(string publisher)
    {
        return FindProperReaderByPublisher(publisher);
    }

    public void ShowReaderByPublisher(string publisher)
    {
        Reader[] readers = FindProperReaderByPublisher(publisher);
        Console.WriteLine("------------");
        foreach (var reader in readers)
        {
            reader.Show();
            Console.WriteLine("");
        }

        Console.WriteLine("------------");
    }

    private Reader[] FindProperReaderByPublisher(string publisher)
    {
        return _elements.FindAll(reader => reader.Publisher == publisher).ToArray();
    }
}