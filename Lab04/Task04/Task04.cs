﻿using Task02.dto;
using Task02.dto.impl;
using Task02.dto.impl.extension;
using Task02.list.impl;

namespace Task04;

public static class Task04
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Введіть кількість старих книжок: ");
        int amountOfOldBooks = int.Parse(Console.ReadLine());
        Console.WriteLine("Введіть кількість електронних книжок: ");
        int amountOfEbooks = int.Parse(Console.ReadLine());
        Console.WriteLine("Введіть кількість газет: ");
        int amountOfNewsPapers = int.Parse(Console.ReadLine());

        Console.WriteLine("Вкажіть імʼя автора 1: ");
        string name = Console.ReadLine();
        Console.WriteLine("Вкажіть прізвище автора 1: ");
        string secondName = Console.ReadLine();

        Console.WriteLine("----------------");

        Console.WriteLine("Вкажіть імʼя автора 2: ");
        string name2 = Console.ReadLine();
        Console.WriteLine("Вкажіть прізвище автора 2: ");
        string secondName2 = Console.ReadLine();


        Author author1 = new Author(name, secondName);
        Author author2 = new Author(name2, secondName2);

        //example of aggregation
        Library library = new Library();

        GenerateOldBooks(amountOfOldBooks, author1, library);
        GenerateEBooks(amountOfEbooks, author2, library);
        GenerateNewsPapers(amountOfNewsPapers, author1, library);

        Console.WriteLine("Масив с елементами: ");
        library.Show();

        Console.WriteLine();

        Console.WriteLine("Виведення за автором Yehor Chevardin");
        library.showByAuthor("Yehor Chevardin");
        
        Console.WriteLine();

        Console.WriteLine("Виведення за автором Anna Sheremet");
        library.showByAuthor("Anna Sheremet");

        Console.WriteLine();

        Console.WriteLine("Виведення книг пілся 2010 року");
        library.showReadersAfterYear(2010);
    }

    static void GenerateOldBooks(int count, Author author, Library library)
    {
        string[] titles = { "Title1", "Title2", "Title3", "Title4", "Title5" };
        string[] publishers = { "PublisherA", "PublisherB", "PublisherC", "PublisherD", "PublisherE" };

        Random random = new Random();

        for (int i = 0; i < count; i++)
        {
            string randomTitle = titles[random.Next(titles.Length)];
            string randomPublisher = publishers[random.Next(publishers.Length)];
            int randomYear = random.Next(2000, 2023);
            int randomPageCount = random.Next(100, 500);
            int randomReprintCount = random.Next(0, 1000);

            //Example of composition
            library.Add(new OldBook(author, randomTitle, randomPublisher, randomYear, randomPageCount,
                randomReprintCount));
        }
    }

    static void GenerateEBooks(int count, Author author, Library library)
    {
        string[] titles = { "Title1", "Title2", "Title3", "Title4", "Title5" };
        string[] publishers = { "PublisherA", "PublisherB", "PublisherC", "PublisherD", "PublisherE" };
        string[] formats = { "pdf", "epub", "txt", "word" };

        Random random = new Random();

        for (int i = 0; i < count; i++)
        {
            string randomTitle = titles[random.Next(titles.Length)];
            string randomPublisher = publishers[random.Next(publishers.Length)];
            int randomYear = random.Next(2000, 2023);
            int randomPageCount = random.Next(100, 500);
            string randomFormat = formats[random.Next(formats.Length)];

            library.Add(new EBook(author, randomTitle, randomPublisher, randomYear, randomPageCount,
                randomFormat));
        }
    }

    static void GenerateNewsPapers(int count, Author author, Library library)
    {
        string[] publishers = { "PublisherA", "PublisherB", "PublisherC", "PublisherD", "PublisherE" };
        string[] types = { "Sport", "News", "Daily news", "Information", "Articles" };

        Random random = new Random();

        for (int i = 0; i < count; i++)
        {
            string randomPublisher = publishers[random.Next(publishers.Length)];
            string randomTypes = types[random.Next(types.Length)];
            int randomPageCount = random.Next(100, 500);
            int randomYear = random.Next(2000, 2023);

            library.Add(new Newspaper(randomTypes, randomPageCount, author, randomPublisher, randomYear));
        }
    }
}