namespace Task02.dto;

public abstract class Reader : Readable, Borrowable
{
    private string _publisher;
    private int _year;
    private Author _author;
    private int _pageCount;
    private bool _isBorrowed;

    public Reader(int pageCount, Author author, string publisher, int year)
    {
        _author = author;
        _publisher = publisher;
        _year = year;
        _pageCount = pageCount;
    }

    public abstract void Read();

    public void Borrow()
    {
        if (_isBorrowed)
        {
            Console.WriteLine("Ця книга вже взята в оренду.");
        }
        else
        {
            _isBorrowed = true;
            Console.WriteLine("Книга взята в оренду.");
        }
    }

    public void Return()
    {
        if (_isBorrowed)
        {
            _isBorrowed = false;
            Console.WriteLine("Книгу повернуто.");
        }
        else
        {
            Console.WriteLine("Ця книга вже є в бібліотеці.");
        }
    }

    public abstract void Show();

    public int PagesCount
    {
        get { return _pageCount; }
        set { _pageCount = value; }
    }

    public bool IsBorrowed
    {
        get { return _isBorrowed; }
    }

    public string Publisher
    {
        get { return _publisher; }
        set { _publisher = value; }
    }

    public int Year
    {
        get { return _year; }
        set { _year = value; }
    }

    public Author Author
    {
        get { return _author; }
        set { _author = value; }
    }
}