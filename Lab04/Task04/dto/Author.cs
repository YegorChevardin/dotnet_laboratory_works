namespace Task02.dto;

public class Author
{
    private string _name;
    private string _secondName;

    public Author(string name, string secondName)
    {
        _name = name;
        _secondName = secondName;
    }

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    public string SecondName
    {
        get { return _secondName; }
        set { _secondName = value; }
    }

    public void Show()
    {
        Console.WriteLine(_name + " " + _secondName);
    }
}