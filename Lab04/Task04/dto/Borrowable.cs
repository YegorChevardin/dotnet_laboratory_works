namespace Task02.dto;

public interface Borrowable
{
    public void Borrow();

    public void Return();
}