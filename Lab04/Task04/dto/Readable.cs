namespace Task02.dto;

public interface Readable
{
    public void Read();
}