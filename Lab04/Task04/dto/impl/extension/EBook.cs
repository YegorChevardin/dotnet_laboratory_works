namespace Task02.dto.impl.extension;

public class EBook : Book
{
    private string _format;
    
    public EBook(Author author, string title, string publisher, int year, int pageCount, string format) : base(author, title,
        publisher, year, pageCount)
    {
        _format = format;
    }

    public string Format
    {
        get { return _format; }
        set { _format = value; }
    }

    public override void Read()
    {
        Console.WriteLine("Electronic format loading");
        base.Read();
    }
}