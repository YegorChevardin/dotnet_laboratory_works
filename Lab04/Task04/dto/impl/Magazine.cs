namespace Task02.dto.impl;

public class Magazine : Reader
{
    private string _name;

    public Magazine(int pageCount, string name, Author author, string publisher, int year) : base(pageCount, author,
        publisher, year)
    {
        _name = name;
    }

    public override void Read()
    {
        Console.WriteLine($"Читається журнал під назвою {_name}");
    }

    public override void Show()
    {
        Console.WriteLine("Інформація про журнал:");
        Console.WriteLine($"Назва: {_name}");
        Console.WriteLine($"Кількість сторінок: {PagesCount}");
    }

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }
}