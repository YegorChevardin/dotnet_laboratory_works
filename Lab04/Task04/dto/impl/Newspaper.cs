namespace Task02.dto.impl;

public class Newspaper : Reader
{
    private string _type;

    public Newspaper(string type, int pagesCount, Author author, string publisher, int year) : base(pagesCount, author,
        publisher, year)
    {
        _type = type;
    }

    public override void Read()
    {
        Console.WriteLine("I am reading news paper... FOO!");
    }

    public override void Show()
    {
        Console.WriteLine("Інформація про газету:");
        Console.WriteLine($"Тип газети: {_type}");
        Console.WriteLine($"Кількість сторінок: {PagesCount}");
    }

    public string Type
    {
        get { return _type; }
        set { _type = value; }
    }
}