namespace Task01;

public class BookList
{
    private List<Book> _books = new List<Book>();

    public BookList(Book[] books)
    {
        _books.AddRange(books);
    }

    public BookList()
    {
    }

    public void Show()
    {
        Console.WriteLine("------------");
        foreach (var book in _books)
        {
            book.Show();
            Console.WriteLine("");
        }

        Console.WriteLine("------------");
    }

    public void AddBook(Book book)
    {
        _books.Add(book);
    }

    public void RemoveBook(Book book)
    {
        _books.Remove(book);
    }

    public Book GetBook(int index)
    {
        return _books[index];
    }

    public void UpdateBook(int index, Book book)
    {
        _books[index] = book;
    }

    public Book[] GetBookByAuthors(string author)
    {
        return BookByAuthor(author);
    }

    public void DeleteByAuthor(string author)
    {
        _books.RemoveAll(book => book.Author == author);
    }

    public Book[] GetAllNotBorrowedBooks()
    {
        return _books.FindAll(book => book.IsBorrowed == true).ToArray();
    }

    private Book[] BookByAuthor(string author)
    {
        return _books.FindAll(book => book.Author == author).ToArray();
    }

    public void showByAuthor(string author)
    {
        Book[] books = BookByAuthor(author);

        Console.WriteLine("------------");
        foreach (var book in books)
        {
            book.Show();
            Console.WriteLine("");
        }

        Console.WriteLine("------------");
    }

    public Book[] FindBooksAfterYear(int year)
    {
        return FindProperBooksAfterYear(year);
    }

    public void showBooksAfterYear(int year)
    {
        Book[] books = FindProperBooksAfterYear(year);
        Console.WriteLine("------------");
        foreach (var book in books)
        {
            book.Show();
            Console.WriteLine("");
        }

        Console.WriteLine("------------");
    }

    private Book[] FindProperBooksAfterYear(int year)
    {
        return _books.FindAll(book => book.Year > year).ToArray();
    }

    public Book[] FindBookByPublisher(string publisher)
    {
        return FindProperBookByPublisher(publisher);
    }

    public void ShowBookByPublisher(string publisher)
    {
        Book[] books = FindProperBookByPublisher(publisher);
        Console.WriteLine("------------");
        foreach (var book in books)
        {
            book.Show();
            Console.WriteLine("");
        }

        Console.WriteLine("------------");
    }

    private Book[] FindProperBookByPublisher(string publisher)
    {
        return _books.FindAll(book => book.Publisher == publisher).ToArray();
    }
}