namespace Task01;

public class Book
{
    private string _author;
    private string _title;
    private string _publisher;
    private int _year;
    private int _pageCount;
    private bool _isBorrowed;

    public Book(string author, string title, string publisher, int year, int pageCount)
    {
        _author = author;
        _title = title;
        _publisher = publisher;
        _year = year;
        _pageCount = pageCount;
        _isBorrowed = false;
    }

    public void Borrow()
    {
        if (_isBorrowed)
        {
            Console.WriteLine("Ця книга вже взята в оренду.");
        }
        else
        {
            _isBorrowed = true;
            Console.WriteLine("Книга взята в оренду.");
        }
    }

    public void Return()
    {
        if (_isBorrowed)
        {
            _isBorrowed = false;
            Console.WriteLine("Книгу повернуто.");
        }
        else
        {
            Console.WriteLine("Ця книга вже є в бібліотеці.");
        }
    }

    public void Show()
    {
        Console.WriteLine("Інформація про книгу:");
        Console.WriteLine($"Автор: {_author}");
        Console.WriteLine($"Назва: {_title}");
        Console.WriteLine($"Видавництво: {_publisher}");
        Console.WriteLine($"Рік видання: {_year}");
        Console.WriteLine($"Кількість сторінок: {_pageCount}");
    }

    public string Author
    {
        get { return _author; }
        set { _author = value; }
    }

    public string Title
    {
        get { return _title; }
        set { _title = value; }
    }

    public string Publisher
    {
        get { return _publisher; }
        set { _publisher = value; }
    }

    public int Year
    {
        get { return _year; }
        set { _year = value; }
    }

    public int PageCount
    {
        get { return _pageCount; }
        set { _pageCount = value; }
    }

    public bool IsBorrowed
    {
        get { return _isBorrowed; }
    }
}