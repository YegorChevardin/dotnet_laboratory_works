﻿namespace Task01
{
    public static class Task01 {
        public static void Main(string[] args)
        {
            Console.WriteLine("Вкажи кількість книг, яку треба згенерувати: ");
            int numberOfBooks = int.Parse(Console.ReadLine());

            BookList bookList = new BookList(GenerateRandomBooks(numberOfBooks));
            
            Console.WriteLine("Згенеровані книги:");
            bookList.Show();
            
            Console.WriteLine();
            
            Console.WriteLine("Виведення за автором Author1");
            bookList.showByAuthor("Author1");
            
            Console.WriteLine();
            
            Console.WriteLine("Виведення книг пілся 2010 року");
            bookList.showBooksAfterYear(2010);
            
            Console.WriteLine();
            
            Console.WriteLine("Виведення книг видавнитства Publisher2");
            bookList.ShowBookByPublisher("Publisher2");
            
            Console.WriteLine();
            
            Console.WriteLine("Додавання нової книги (Author = Yehor)");
            bookList.UpdateBook(0, new Book("Yehor", "Some", "Somesome", 2004, 100));
            
            Console.WriteLine("Виведення за автором Yehor");
            bookList.showByAuthor("Yehor");
        }
        
        static Book[] GenerateRandomBooks(int count)
        {
            string[] authors = { "Author1", "Author2", "Author3", "Author4", "Author5" };
            string[] titles = { "Title1", "Title2", "Title3", "Title4", "Title5" };
            string[] publishers = { "PublisherA", "PublisherB", "PublisherC", "PublisherD", "PublisherE" };

            Random random = new Random();
            List<Book> randomBooks = new List<Book>();

            for (int i = 0; i < count; i++)
            {
                string randomAuthor = authors[random.Next(authors.Length)];
                string randomTitle = titles[random.Next(titles.Length)];
                string randomPublisher = publishers[random.Next(publishers.Length)];
                int randomYear = random.Next(2000, 2023); // Random year between 2000 and 2022
                int randomPageCount = random.Next(100, 500); // Random page count between 100 and 499

                randomBooks.Add(new Book(randomAuthor, randomTitle, randomPublisher, randomYear, randomPageCount));
            }

            return randomBooks.ToArray();
        }

    }
}

