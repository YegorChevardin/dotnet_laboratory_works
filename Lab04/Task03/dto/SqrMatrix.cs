namespace Task03.dto;

class SqrMatrix
{
    private int[,] matrix;
    public int Size { get; private set; }

    public SqrMatrix(int size)
    {
        Size = size;
        matrix = new int[size, size];
    }

    public SqrMatrix(SqrMatrix other)
    {
        Size = other.Size;
        matrix = new int[Size, Size];
        Array.Copy(other.matrix, matrix, other.matrix.Length);
    }

    public int this[int row, int col]
    {
        get { return matrix[row, col]; }
        set { matrix[row, col] = value; }
    }

    public static SqrMatrix operator +(SqrMatrix a, SqrMatrix b)
    {
        if (a.Size != b.Size)
            throw new ArgumentException("Матриці мають різний розмір");

        SqrMatrix result = new SqrMatrix(a.Size);
        for (int i = 0; i < a.Size; i++)
        {
            for (int j = 0; j < a.Size; j++)
            {
                result[i, j] = a[i, j] + b[i, j];
            }
        }

        return result;
    }

    public static SqrMatrix operator -(SqrMatrix a, SqrMatrix b)
    {
        if (a.Size != b.Size)
            throw new ArgumentException("Матриці мають різний розмір");

        SqrMatrix result = new SqrMatrix(a.Size);
        for (int i = 0; i < a.Size; i++)
        {
            for (int j = 0; j < a.Size; j++)
            {
                result[i, j] = a[i, j] - b[i, j];
            }
        }

        return result;
    }

    public static SqrMatrix operator *(SqrMatrix a, SqrMatrix b)
    {
        if (a.Size != b.Size)
            throw new ArgumentException("Матриці мають різний розмір");

        SqrMatrix result = new SqrMatrix(a.Size);
        for (int i = 0; i < a.Size; i++)
        {
            for (int j = 0; j < a.Size; j++)
            {
                for (int k = 0; k < a.Size; k++)
                {
                    result[i, j] += a[i, k] * b[k, j];
                }
            }
        }

        return result;
    }

    public void CopyFrom(SqrMatrix other)
    {
        if (Size != other.Size)
            throw new ArgumentException("Матриці мають різний розмір");

        for (int i = 0; i < Size; i++)
        {
            for (int j = 0; j < Size; j++)
            {
                this[i, j] = other[i, j];
            }
        }
    }

    public double Norm()
    {
        double norm = 0;
        for (int i = 0; i < Size; i++)
        {
            for (int j = 0; j < Size; j++)
            {
                norm += Math.Abs(matrix[i, j]);
            }
        }

        return norm;
    }

    public void Print()
    {
        for (int i = 0; i < Size; i++)
        {
            for (int j = 0; j < Size; j++)
            {
                Console.Write(matrix[i, j] + "\t");
            }

            Console.WriteLine();
        }
    }
    
    public static void SquareMatrix(SqrMatrix[] matrices)
    {
        foreach (var matrix in matrices)
        {
            for (int i = 0; i < matrix.Size; i++)
            {
                for (int j = 0; j < matrix.Size; j++)
                {
                    matrix[i, j] *= matrix[i, j];
                }
            }
        }
    }
}