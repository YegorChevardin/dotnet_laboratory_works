﻿using Task03.dto;

namespace Task03;

public static class Task03
{
    public static void Main(string[] args)
    {
        Random random = new Random();
        
        int randomMatrixSize = random.Next(3, 9);
        SqrMatrix a = new SqrMatrix(randomMatrixSize);
        SqrMatrix b = new SqrMatrix(randomMatrixSize);

        initMatrix(a, random);
        initMatrix(b, random);

        Console.WriteLine("Матриця A:");
        a.Print();

        Console.WriteLine("Матриця B:");
        b.Print();

        SqrMatrix c = a + b;
        Console.WriteLine("Результат додавання матриць A і B:");
        c.Print();

        SqrMatrix d = a - b;
        Console.WriteLine("Результат віднімання матриць A і B:");
        d.Print();

        SqrMatrix e = a * b;
        Console.WriteLine("Результат множення матриць A і B:");
        e.Print();
        
        double norm = a.Norm();
        Console.WriteLine($"Норма матриці A: {norm}");
        
        SqrMatrix f = new SqrMatrix(a);
        Console.WriteLine("Матриця F (копія матриці A):");
        f.Print();
        
        Console.WriteLine("Взведення масиву матриць в квадрат");
        
        SqrMatrix[] matrices = new SqrMatrix[3];
        matrices[0] = new SqrMatrix(randomMatrixSize);
        matrices[1] = new SqrMatrix(randomMatrixSize);
        matrices[2] = new SqrMatrix(randomMatrixSize);

        // Ініціалізація матриць matrices[0], matrices[1], matrices[2]
        foreach (var matrixElement in matrices)
        {
            initMatrix(matrixElement, random);
        }
        
        // Виклик функції для зведення матриць в квадрат
        SqrMatrix.SquareMatrix(matrices);

        // Виведення змінених матриць
        Console.WriteLine("Матриця matrices[0] після зведення в квадрат:");
        matrices[0].Print();

        Console.WriteLine("Матриця matrices[1] після зведення в квадрат:");
        matrices[1].Print();

        Console.WriteLine("Матриця matrices[2] після зведення в квадрат:");
        matrices[2].Print();
    }

    private static void initMatrix(SqrMatrix matrix, Random random)
    {
        for (int i = 0; i < matrix.Size; i++)
        {
            for (int j = 0; j < matrix.Size; j++)
            {
                int randomA = random.Next(10, 50);
                matrix[i, j] = randomA;
            }
        }
    }
}
